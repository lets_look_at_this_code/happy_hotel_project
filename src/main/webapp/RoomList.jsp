<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico">
<link rel="Shortcut Icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="lib/ /1.0.8/iconfont.css" />

<title>客户消费商品列表</title>
</head>
<body>
	<nav class="breadcrumb">
		首页 <span class="c-gray en">&gt;</span>客房管理<span class="c-gray en">&gt;</span>客房类型管理<span
			class="c-gray en">&gt;</span> 同类型房间列表
	</nav>
	<div class="cl pd-5 bg-1 bk-gray mt-20">
		<span class="l"><a href="addRoom.jsp"
			class="btn btn-primary radius"> 添加房间</a> </span>
	</div>
	<div class="page-container">
		<table class="table table-border table-bordered table-bg">
			<thead>
				<tr>
					<th scope="col" colspan="6">同类型房间列表</th>
				</tr>
				<tr class="text-c">
					<th width="25"><input type="checkbox" name="" value=""></th>
					<th width="80">房间号</th>
					<th width="80">类型ID</th>
					<th width="80">房间状态</th>
					<th width="80">备注</th>
					<th width="80">操作</th>

				</tr>
			</thead>
			<tbody id="saleDt">
				<c:forEach items="${roomList }" var="room" varStatus="status">
					<tr class="text-c">
						<td><input type="checkbox" value="${status.count }" name=""></td>
						<td>${room.roomNo }</td>
						<td>${room.typeId }</td>
						<td>${room.state }</td>
						<td>${room.remark }</td>

						<td class="td-manage"><a style="text-decoration: none"
							onClick="" href="javascript:;" title="修改">修改</a> &nbsp;/&nbsp; <a
							href="RoomServlet?action=delete&roomNo=${room.roomNo }"
							class="ml-5 confirm" style="text-decoration: none">删除</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!--_footer 作为公共模版分离出去-->
	<script type="text/javascript" src="jquery/jquery.min.js"></script>
	<script type="text/javascript" src="jquery/layer.js"></script>
	<script type="text/javascript" src="jquery/H-ui.min.js"></script>
	<script type="text/javascript" src="jquery/H-ui.admin.js"></script>
	<!--/_footer 作为公共模版分离出去-->

	<script type="text/javascript">
		$(function() {
			
			$(".confirm").click(function() {
				return confirm("您确定要删除这条记录吗？");
			});
		});
	</script>
</body>
</html>