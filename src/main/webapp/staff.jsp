<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.min.css" id="skin" />
<link rel="stylesheet" type="text/css" href="css/skin.css"/>
<link rel="stylesheet" type="text/css" href="css/layer.css"/>
<link rel="stylesheet" type="text/css" href="css/layer1.css"/>
<link rel="stylesheet" type="text/css" href="css/H-ui.reset.css"/>
<link rel="stylesheet" type="text/css" href="css/H-ui.ie.css"/>
<title></title>
<style>
	@font-face{
    	font-family:"悦黑体";
    	src : url("img/MFYueHei_Noncommercial-Regular.otf");
	}	
	*{
		font-family: "悦黑体";
	}
	.img1{
		margin-left:25px;
	}
	
	.img2{
		margin-left:45px;
	}
	.img3{
		margin-left:250px;
	}
	.img4{
		margin-left:120px;
	}
	.btn btn-danger radius{
		position: relative;
		top:100px;
		left:10px;
	}
	.a1{
		display:block;
		width:100px;
		height:31px;
		border-radius:5px;
		background-color: #dd514c;
		color: #ffffff;
		font-size:15px;
		line-height:31px;
		text-align: center;
		position: relative;
		top:15px;
		left:1340px;
	}
	#a2{
		position: relative;
		top:-15px;
		left:1480px;
	}
	#a3{
		position: relative;
		top:-46px;
		left:1620px;
	}
	#a4{
		position: relative;
		top:-76px;
		left:1760px;
	}
	#dept{
		position: relative;
		top:-75px;
	}
	border{
		border:1px solid #5cf9f3;
		
	}
</style>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>员工 <span class="c-gray en">&gt;</span>员工管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="StaffServlet?action=show" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
	<div class="text-c">
		<form action="StaffServlet?action=show" method="post">
		<input type="text" class="input-text" style="width:250px" value="${searchs }" id="" name="searchs">
		<button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
		</form>
	</div>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
	<a href="javascript:;" onclick="addStaff()" style="margin-left:20px" class="btn btn-danger radius" ><i class="Hui-iconfont">&#xe600;</i>添加员工</a>
	<a href="javascript:;" onclick="addDepartment()" style="margin-left:20px" class="btn btn-danger radius" ><i class="Hui-iconfont">&#xe600;</i>添加部门</a></span> <span class="r">共有数据：<strong>${num }</strong> 条</span> </div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-hover table-bg table-sort" style="font-family: '悦黑体'">
			<thead>
				<tr class="text-c">
					<th width="25"><input type="checkbox" class="checkbox1"></th>
					<th width="60">ID</th>
					<th width="60">员工姓名</th>
					<th>所属部门</th>
					<th>工资</th>
					<th width="100">操作</th>
				</tr>
				<c:forEach items="${list}" var="list">
				<tr>
					<td width="25"><input type="checkbox" class="checkbox1"></td>
					<td width="60">${list.staffId}</td>
					<td width="60">${list.staffName}</td>
					<td>${list.department.deptName}</td>
					<td>${list.salary }</td>
					<td width="200"><img class="img1" id="${list.staffId }" src="img/Image1.png"><img class="img2" id="${list.staffId }" src="img/Image2.png"></td>
				</tr>
				</c:forEach> 
		</table>
		<a href="javascript:;" onclick="index(${pageIndex},${totalPages },'shouye')" class="a1"><i class="Hui-iconfont">&#xe625;</i> 首页</a>
		<a href="javascript:;" onclick="index(${pageIndex},${totalPages },'shangyiye')" class="a1" id="a2"><i class="Hui-iconfont">&#xe678;</i> 上一页</a>
		<a href="javascript:;" onclick="index(${pageIndex},${totalPages },'xiayiye')" class="a1" id="a3"><i class="Hui-iconfont">&#xe67a;</i> 下一页</a>
		<a href="javascript:;" onclick="index(${pageIndex},${totalPages },'weiye')" class="a1" id="a4"><i class="Hui-iconfont">&#xe67f;</i> 尾页</a>
	</div>
	<div class="mt-20" id="dept">
		<table class="table table-border table-bordered table-hover table-bg table-sort">
			<thead>
				<tr class="text-c">
					<th width="25"><input type="checkbox" class="checkbox2"></th>
					<th width="60">ID</th>
					<th width="60">部门</th>
					<th width="100">操作</th>
				</tr>
				<c:forEach items="${deptlist }" var="list">
				<tr>
					<td width="15%"><input type="checkbox" class="checkbox2"></td>
					<td width="15%">${list.deptId}</td>
					<td width="30%">${list.deptName}</td>
					<td width="40%"><img class="img3" id="${list.deptId }" src="img/Image1.png"><img class="img4" id="${list.deptId }" style="m" src="img/Image2.png"></td>
				</tr>
				</c:forEach>
		</table>
	</div>
</div>
<script type="text/javascript" src="jquery/jquery.min.js"></script> 
<script type="text/javascript" src="jquery/layer.js"></script>  
<script type="text/javascript" src="jquery/H-ui.min.js"></script> 
<script type="text/javascript" src="jquery/H-ui.admin.js"></script>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script> 

<script type="text/javascript" src="jquery/WdatePicker.js"></script>
<script type="text/javascript" src="jquery/WdatePicker.js"></script> 
<script type="text/javascript" src="jquery/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="jquery/laypage.js"></script>
<script type="text/javascript">
$(function(){
	$(".img1").click(function(){
		var id = $(this).attr('id');
		layer.open({
             type: 2,
             title: '修改信息',
             area: ['1000px','500px'],
             fixed:false,
             maxmin: true,
             shade: 0.8,
             content:['StaffServlet?action=updateStaff&operation=update&id='+id,'no'],
             end: function () {
                 location.reload();
             }
		 });  
	});//layer.msg('修改成功',{shade: 0.3});
});
function flush(){
    layer.msg('成功',{shade: 0.3});
    window.location.href="StaffServlet?action=show&pageIndex=1"
} 
function addStaff(){
	layer.open({
    	type: 2,
        title: '添加员工',
        area: ['1000px','500px'],
        fixed:false,
        maxmin: true,
        shade: 0.5,
        content:['StaffServlet?action=addStaff&operation=addStaff&','no']
	}); 
		  /* layer.msg('修改成功',{shade: 0.3}); */
}
		//layer.msg('修改成功',{shade: 0.3});
function addDepartment(){
	 layer.open({
         type: 2,
         title: '添加部门',
         area: ['1000px','500px'],
         fixed:false,
         maxmin: true,
         shade: 0.5,
         content:['updateDepartment.jsp?action=addDepartment&str=添加&','no']
		 }); 	
}
$(function(){
	$(".img2").click(function(){
			var id = $(this).attr('id');
			layer.confirm('确认要删除吗？',function(){
	 			$.ajax({
	 				url:"StaffServlet?action=deleteStaff&id="+id,
	 				type:"get",
	 				dataType:"text",
	 				success:function(result){
	 					layer.msg(result,{shade:0.3});
	 				}
	 		});
		});
	});
});
$(function(){
	$(".img4").click(function(){
			var deptid = $(this).attr('id');
			layer.confirm('确认要删除吗？',function(){
	 			$.ajax({
	 				url:"StaffServlet?action=deleteDepartment&deptid="+deptid,
	 				type:"get",
	 				dataType:"text",
	 				success:function(result){
	 					layer.msg(result,{shade:0.3});
	 				}
	 		});
		});
	});
});
$(function(){
	$(".img3").click(function(){
		var deptid = $(this).attr('id');
		var index = layer.open({
             type: 2,
             title: '修改部门',
             area: ['1000px','500px'],
             fixed:false,
             maxmin: true,
             shade: 0.8,
             content:['updateDepartment.jsp?action=updateDepartment&str=修改&deptid='+deptid,'no']		 
		 });  
	});//layer.msg('修改成功',{shade: 0.3});
});
$(function(){  
    $("tr").hover(function(){
    	$(this).css('height','80px');
    	$(this).css('background-color','#84f8e9');
    },function(){
    	$(this).css('height','40px');
    	$(this).css('background-color','#ffffff');
    });
}) ;  
$(function(){
	$(".checkbox1").toggle(
	function(){
		$(".checkbox1").attr("checked","true"); 
	},
	function(){
		$(".checkbox1").removeAttr("checked"); 
	});
	$(".checkbox2").toggle(
	function(){
		$(".checkbox2").attr("checked","true"); 
	},
	function(){
		$(".checkbox2").removeAttr("checked"); 
	});
});
function index(pageIndex,totalPages,action){
	var value = $(".input-text").val();
	if("shouye" == action){
		window.location.href="StaffServlet?action=show&pageIndex=1&searchs="+value;
		return;
	}else if("shangyiye" == action){
		var pageIndexs = pageIndex - 1;
		window.location.href="StaffServlet?action=show&pageIndex="+pageIndexs+"&searchs="+value;
		return;
	}else if("xiayiye" == action){
		var pageIndexs = pageIndex + 1;
		window.location.href="StaffServlet?action=show&pageIndex="+pageIndexs+"&searchs="+value;
		return;
	}else if("weiye" == action){
		window.location.href="StaffServlet?action=show&pageIndex="+totalPages+"&searchs="+value;
		return;
	}
}
/*  $(function(){
	$('.table-sort').dataTable({
		"aaSorting": [[ 1, "desc" ]],//默认第几个排序
		"bStateSave": true,//状态保存
		"aoColumnDefs": [
		  //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
		  {"orderable":false,"aTargets":[0,2,4]}// 制定列不参与排序
		]
	});

});

function member_add(title,url,w,h){
	layer_show(title,url,w,h);
}

function member_show(title,url,id,w,h){
	layer_show(title,url,w,h);
}

function member_stop(obj,id){
	layer.confirm('确认要停用吗？',function(index){
		$(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe6e1;</i></a>');
		$(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
		$(obj).remove();
		layer.msg('已停用!',{icon: 5,time:1000});
	});
}


function member_start(obj,id){
	layer.confirm('确认要启用吗？',function(index){
		$(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_stop(this,id)" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a>');
		$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
		$(obj).remove();
		layer.msg('已启用!',{icon: 6,time:1000});
	});
}

function member_edit(title,url,id,w,h){
	layer_show(title,url,w,h);
}

function change_password(title,url,id,w,h){
	layer_show(title,url,w,h);	
}

  */  
</script>
</body>
</html>