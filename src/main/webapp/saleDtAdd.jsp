<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />

<link rel="stylesheet" type="text/css" href="css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css"
	href="lib/Hui-iconfont/1.0.8/iconfont.css" />

<title>添加销售商品</title>
</head>
<body>
	<nav class="breadcrumb">
		首页 <span class="c-gray en">&gt;</span>客户住宿管理<span class="c-gray en">&gt;</span>客户消费商品管理<span
			class="c-gray en">&gt;</span>添加客户消费商品
	</nav>
	<article class="page-container">
		<form action="SaleDtServlet?action=add" method="post"
			class="form form-horizontal" id="form-admin-add">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>商品类别</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box" style="width: 150px;"> <select
						class="select" id="typeId" name="goodsType" size="1">

					</select>
					</span>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>商品名称</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box" style="width: 150px;"> <select
						class="select" id="goodsId" name="goodsName" size="1">
							
					</select>
					</span>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>消费数量</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" id="num" name="num"
						style="width: 150px;">
				</div>
			</div>

			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit"
						value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>

	<!--_footer 作为公共模版分离出去-->
	<script type="text/javascript" src="jquery/jquery.min.js"></script>
	<script type="text/javascript" src="jquery/H-ui.min.js"></script>
	<script type="text/javascript" src="jquery/H-ui.admin.js"></script>
	<!--/_footer 作为公共模版分离出去-->

	<!--请在下方写此页面业务相关的脚本-->

	<script type="text/javascript">
		$(function() {

			$.ajax({
				type :'post',
				url : "GoodsTypeServlet?action=goodsTypeAjax",
				success : function(data) {
					var goodsTypeData = data.split("-");//处理返回参数，分割成字符串数组
					//将数组中的每个字符串分割成一个字符串数组，取分割后的第一个字符串组成一个新的商品类型数组
					var typeIds = goodsTypeData[0].split(":");
					//将数组中的每个字符串分割成一个字符串数组，取分割后的第二个字符串组成一个新的类型名称数组
					var typeNames = goodsTypeData[1].split(":");
					var option = "<option value=''>请选择商品类型</option>";//填写下拉列表的默认选项
					$.each(typeIds,function(i,typeId){
						if(i==typeIds.length-1){
							return;
						}
						option += "<option value="+typeId+">"+typeNames[i]+"</option>";
					});
					$("#typeId").append(option);
				}
			});
			
			$("#typeId").change(function(){
				$("#goodsId").empty;
				var typeId = $(this).val();
				alert(typeId);
				$.ajax({
					type :'post',
					url : "GoodsServlet",
					data : "{action:ajaxSelect,typeId:"+typeId+"}",
					success : function(data) {
						var goodsData = data.split("-");//处理返回参数，分割成字符串数组
						//将数组中的每个字符串分割成一个字符串数组，取分割后的第一个字符串组成一个新的商品类型数组
						var goodsIds = goodsData[0].split(":");
						//将数组中的每个字符串分割成一个字符串数组，取分割后的第二个字符串组成一个新的类型名称数组
						var goodsNames = goodsData[1].split(":");
						var goodsOption = "<option value=''>请选择商品</option>";//填写下拉列表的默认选项
						$.each(goodsIds,function(i,goodsId){
							if(i==goodsIds.length-1){
								return;
							}
							goodsOption += "<option value="+goodsId+">"+goodsNames[i]+"</option>";
						});
						$("#goodsId").append(goodsOption);
					}
				}); 
				 
			});
			
		});
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>