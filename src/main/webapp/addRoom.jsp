<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />

<link rel="stylesheet" type="text/css" href="css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css"
	href="lib/Hui-iconfont/1.0.8/iconfont.css" />

<title>添加销售商品</title>
</head>
<body>
	<nav class="breadcrumb">
		首页 <span class="c-gray en">&gt;</span>客户住宿管理<span class="c-gray en">&gt;</span>客户消费商品管理<span
			class="c-gray en">&gt;</span>添加客户消费商品
	</nav>
	<article class="page-container">
		<form action="SaleDtServlet?action=add" method="post"
			class="form form-horizontal" id="form-admin-add">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>房间号</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" id="roomNo"
						name="roomNo" style="width: 300px;">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>房间类型</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box" style="width: 300px;">
						<select class="select" name="roomType" size="1">
							<c:forEach items="${roomTypeLists}" var="type">
								<option value="${type.typeId }">${type.typeName }</option>
							</c:forEach>
						</select>
					</span>
				</div>
			</div>
			<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">备注：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<textarea name="remark" cols="20" rows="3" class="textarea"  placeholder="" datatype="*0-100" dragonfly="true" style="width: 300px;" ></textarea>
				
			</div>
		</div>

			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit"
						value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>

	<!--_footer 作为公共模版分离出去-->
	<script type="text/javascript" src="jquery/jquery.min.js"></script>
	<script type="text/javascript" src="jquery/H-ui.min.js"></script>
	<script type="text/javascript" src="jquery/H-ui.admin.js"></script>
	<!--/_footer 作为公共模版分离出去-->

	<!--请在下方写此页面业务相关的脚本-->

	<script type="text/javascript">
		$(function() {

			$(form).ajaxSubmit({
				type : 'post',
				url : "xxxxxxx",
				success : function(data) {

				}
			});
		});
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>