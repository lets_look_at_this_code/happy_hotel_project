<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
<script type="text/javascript" src="jquery/layer.js"></script>
<script type="text/javascript" src="jquery/laypage.js"></script>
<script type="text/javascript">
$(function(){
	$(".submit").click(function(index){
		window.parent.flush();
	});	
});
$(function(){
	$(".a1").blur(function(){
		var val = $(this).val();
		var str = $(".submit").val();
		if(val == ""){
			$(".ss1").html('不能为空');
			$(".submit").val("不能提交");
			$(".submit").css("background-color","#b8b8b8");
		}else{
			$(".ss1").html('');
			$(".submit").val(str);
			$(".submit").css("background-color","#53e474");
		}
	});
	$(".a3").blur(function(){
		var val = $(this).val();
		var str = $(".submit").val();
		if(val == ""){
			$(".ss2").html('不能为空');
			$(".submit").val("不能提交");
			$(".submit").css("background-color","#b8b8b8");
		}else{
			$(".ss1").html('');
			$(".submit").val(str);
			$(".submit").css("background-color","#53e474");
		}
	});
	$(".a").blur(function(){
 		var username = $(".a").val();
 		$.ajax({
 			url:"Servlet",
 			type:"get",
 			data:"name="+username,
 			dataType:"json",
 			success:function(result){
 				$(".span").html(result.login);
 				$(".img").attr("src",result.src);	
 				$(".img").show();
 			}
 		});
 	});	
});
</script>
<style>
	@font-face{
    	font-family:font;
   	 	src : url("img/senlin.ttf"); 
	}	
	div{
		width: 400px;
		height:400px;
		margin:100px auto;
		font-family:font;
		font-size:20px;
	}
	.s2{
		position:relative;
		top:30px;
	}
	.s3{
		position:relative;
		top:60px;
	}
	input[type="submit"]{
		position: relative;
		top:150px;
		left:-50px;
		width:100%;
		height: 50px;
		border-radius:25px;
		font-size: 40px;
		font-family:font;
		border-color:#53e474;
		background-color:#53e474;
	}
	.ss1,.ss2{
		font-family:font;
		color:#dd514c;
		margin-left: 10px;
	}
	.ss2{
		position: relative;
		top:62px;
	}
</style>
</head>
<body>
	<div>
		<form action="../StaffServlet?action=updateStaffSubmit&id=${param.id }&operation=${param.operation }" method="post">
			<span class="s1">员工姓名:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="name" class="a1" class="null"><span class="ss1"></span></span></br>
			<span class="s2">员工所属部门:
			  <select name="dept" style="width:175px">
				<c:forEach items="${list }" var="list">
					<option value="${list.deptId }">${list.deptName }</option>
				</c:forEach>
              </select>
			</span></br>
			<span class="s3">员工工资:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="salary" class="a3" class="null"/></span><span class="ss2"></span>
			<input type="submit" value="${param.str }" onclick="submit()" class="submit"/>
		</form>
	</div>
</body>
</html>