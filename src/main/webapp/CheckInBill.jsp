<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />

<link rel="stylesheet" type="text/css" href="css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="css/H-ui.admin.css" />
<title>住宿登记列表</title>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
<script type="text/javascript">
	$(function(){
		var state = "${state}";
		$("#state").val(state);
		var startDate = "${startDate}"
		$("#logmin").val(startDate);
		var endDate = "${endDate}"
		$("#logmax").val(endDate);
		var roomNo = "${roomNo}";
		$("#roomNo").val(roomNo);
		
		$("#submit").click(function(){
			var state1 = $("#state").val();
			var sDate = $("#logmin").val();
			var eDate = $("#logmax").val();
			var roomNo1 =$("#roomNo").val();
			$.ajax({
				url:CheckInServlet?action=showCheckIn,
				type:"get",
				date:{"state":state1,"startDate":sDate,"endDate":eDate,"roomNo":roomNo1},
				dateType:
			});
		});
	});
	
</script>
</head>
<body>
<div class="page-container">
	<div class="text-c">
	 <span class="select-box inline">
		单据状态：<select name="state" class="select" id="state">
			<option value="0">未退房</option>
			<option value="1">已退房</option>
			<option value="2">全部单据</option>
		</select>
		</span> &nbsp; 日期范围：
		<input type="date"  id="logmin" class="input-text Wdate" name="startDate" style="width:150px;">
		-
		<input type="date"  id="logmax" class="input-text Wdate" name="endDate" style="width:150px;">
		<input type="text" name="roomNo" id="roomNo" placeholder=" 房间号" style="width:150px" class="input-text">
		<button name="" id="submit" class="btn btn-success" type="submit"> 搜单据</button>
	</div>
	<div class="cl pd-5 bg-1 bk-gray mt-20">
	 <a class="btn btn-primary radius" data-title="登记/预定" data-href="article-add.html" onclick="Hui_admin_tab(this)" href="javascript:;"></i> 登记/预定</a></span>
	  <span class="r">共有数据：<strong>${count}</strong> 条</span> </div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort table-responsive">
			<thead>
				<tr class="text-c">
					<th width="60">单据编号</th>
					<th width="30">单据状态</th>
					<th width="30">顾客姓名</th>
					<th width="20">性别</th>
					<th width="100">身份证号</th>
					<th width="120">地址</th>
					<th width="60">手机号</th>
					<th width="30">是否会员</th>
					<th width="40">房间号</th>
					<th width="30">价格</th>
					<th width="100">入住时间</th>
					<th width="30">入住天数</th>
					<th width="100">预计退房时间</th>
					<th width="30">押金</th>
					<th width="100">顾客留言</th>					
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${checkInList}" var="ckeckIn">
					<th width="60">${ckeckIn.billNo}</th>
					<th width="30">
						<c:choose>
							<c:when test="${ckeckIn.state == 0}">未退房</c:when>
							<c:when test="${ckeckIn.state == 1}">已退房</c:when>
						</c:choose>
					</th>
					<th width="30">${ckeckIn.clientName}</th>
					<th width="20">
						<c:choose>
							<c:when test="${ckeckIn.sex == 0}">女</c:when>
							<c:when test="${ckeckIn.sex == 1}">男</c:when>
						</c:choose>
					</th>
					<th width="100">${ckeckIn.identity}</th>
					<th width="120">${ckeckIn.address}</th>
					<th width="60">${ckeckIn.phone}</th>
					<th width="30">
						<c:choose>
							<c:when test="${ckeckIn.isVip == 0}">否</c:when>
							<c:when test="${ckeckIn.isVip == 1}">是</c:when>
						</c:choose>
					</th>
					<th width="40">${ckeckIn.roomNo}</th>
					<th width="30">${ckeckIn.price}</th>
					<th width="100"><fmt:formatDate value="${ckeckIn.inTime}" type="both"/></th>
					<th width="30">${ckeckIn.days}</th>
					<th width="100"><fmt:formatDate value="${ckeckIn.outTime}" type="both"/></th>
					<th width="30">${ckeckIn.deposit}</th>
					<th width="100">${ckeckIn.notes}</th>
				</c:forEach>
				<tr>
					<th colspan="15" align="center">
						<a href="CheckInServlet?action=showCheckIn&pageIndex=1&state=${state}&startDate=${startDate}&endDate=${endDate}&roomNo=${roomNo}">首页</a>&nbsp;&nbsp;
						<a href="CheckInServlet?action=showCheckIn&pageIndex=${pageIndex-1}&state=${state}&startDate=${startDate}&endDate=${endDate}&roomNo=${roomNo}">上一页</a>&nbsp;&nbsp;
						<a href="CheckInServlet?action=showCheckIn&pageIndex=${pageIndex+1}&state=${state}&startDate=${startDate}&endDate=${endDate}&roomNo=${roomNo}">下一页</a>&nbsp;&nbsp;
						<a href="CheckInServlet?action=showCheckIn&pageIndex=${totalPages}&state=${state}&startDate=${startDate}&endDate=${endDate}&roomNo=${roomNo}">尾页</a>
					</th>
				</tr>
			</tbody>
		</table>
	</div>
</div>

</body>
</html>