<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>入住登记</title>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
<script type="text/javascript">
	$(function(){
		var isVip = "${bookRoom.isVip}";
		if(isVip == 1){
			$("#shi").Attr("checked","checked");
		}else if(isVip == 0){
			$("#fou").Attr("checked","checked");
		}	
	});
</script>
</head>
<body>
	<form action="CheckInServlet?action=addCheckIn&bookBillNo=${bookRoom.billNo}" method="post">
		姓名：<input type="text" name="clientName" id="clientName" value="${bookRoom.clientName}"><br>
		性别：<input type="radio" name="sex" id="nan" value="1">男
			<input type="radio" name="sex" id="nv" value="0">女<br>
		身份证：<input type="text" name="identity" id="identity"><br>
		地址：<input type="text" name="address" id="address"><br>
		手机号：<input type="text" name="phone" id="phone" value="${bookRoom.phone}"><br>
		是否会员：<input type="radio" name="isVip" id="shi" value="1">是
			   <input type="radio" name="isVip" id="fou" value="0">否<br>
		房间号：<input type="text" name="roomNo" id="roomNo" value="${bookRoom.roomNo}"><br>
		入住天数：<input type="text" name="days" id="days" value="${bookRoom.days}">
		价格：<input type="text" name="price" id="price" value="${bookRoom.price}"><br>
		押金：<input type="text" name="deposit"><br>
		留言：<input type="text" name="notes"><br>
		<input type="submit" value="登记">
	</form>
</body>
</html>