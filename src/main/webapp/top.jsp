<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.min.css" id="skin" />
<style type="text/css">
	*{
		margin: 0px;
		padding: 0px;
	}
	.div1{
		width: 100%;
		height:50px;
		background-color: #0c0000;
	}
	.div2{
		color: #e8f2fe;
		position: relative;
		top:15px;
		left:1500px;
		font-size:20px; 
	}
	.div3{
		display:none;
		color: #b4b4b4;
		position:relative;
		top:9px;
		left:-7px;
		width:120px;
		font-size: 15px;	
	}
	ul{
		list-style: none;
		background-color:#ffffff
	}
	li{
		display: block;	
		background-color:#ffffff;
		font-size:15px;
		width: 100%;
		transition: all 0.6s;
		margin: 10px;
	}
	li:hover{
		transform: scale(1.2);
	}
</style>
</head>
<body>
	<div class="div1">
		<div class="div2">
			<i class="Hui-iconfont">&#xe62d;</i>&nbsp;&nbsp;${user.userName}&nbsp;&nbsp;<i id="i1" class="Hui-iconfont">&#xe6d5;</i>
			<div class="div3">
				<ul>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;<i class="Hui-iconfont">&#xe627;</i>&nbsp;&nbsp;&nbsp;<a>账号信息</a></li>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;<i class="Hui-iconfont">&#xe647;</i>&nbsp;&nbsp;&nbsp;<a>修改密码</a></li>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;<i class="Hui-iconfont">&#xe726;</i>&nbsp;&nbsp;&nbsp;<a>注销</a></li>
			</ul>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
<script type="text/javascript">
$(function(){
	$("li").hover(function(){
		$(this).css("background-color","#0c0000");
	},function(){
		$(this).css("background-color","#ffffff");
	});
	$("#i1").toggle(function() {
		$(".div3").slideDown('1000');
	}, function() {
		$(".div3"). slideUp('1000');
	});
});
</script>
</html>