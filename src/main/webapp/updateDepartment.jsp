<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="jquery/jquery-1.8.3.js"></script>
<script type="text/javascript" src="jquery/layer.js"></script>
<script type="text/javascript" src="jquery/laypage.js"></script>
<script type="text/javascript">
$(function(){
	$(".submit").click(function(index){
		window.parent.flush();
	});	

	$(".a3").blur(function(){
		var val = $(this).val();
		var str = $(".submit").val();
		if(val == ""){
			$(".submit").css("background-color","#b8b8b8");
		}else{
			$(".submit").css("background-color","#53e474");
		}
	});
});
</script>
<style>
	@font-face{
    font-family:"悦黑体";
    src : url("img/MFYueHei_Noncommercial-Regular.otf");
	}	
	div{
		width: 400px;
		height:400px;
		margin:100px auto;
		font-family:悦黑体;
		font-size:20px;
	}
	.s2{
		position: relative;
		top:30px;
	}
	.s3{
		position: relative;
		top:60px;
	}
	input[type="submit"]{
		position: relative;
		top:150px;
		left:-50px;
		width:100%;
		height: 50px;
		border-radius:25px;
		font-size: 40px;
		font-family:"悦黑体";
		background-color:  #e8f2fe;
		border-color:#53e474;
		background-color:#53e474;
	}
</style>
</head>
<body>
	<div>
		<form action="../StaffServlet?action=${param.action }&deptid=${param.deptid }" method="post">
			</span></br>
			<span class="s3">部门名称:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="deptname" class="a3"/></span>
			<input type="submit" value="${param.str }" class="submit"/>
		</form>
	</div>
</body>
</html>