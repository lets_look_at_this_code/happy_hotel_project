package cn.hotel.entity;

import java.util.Date;
//会员充值表
public class Vipcharge {
	private int phone;//手机号
	private double amt;//充值金额
	private Date amttime;//充值时间
	
	public Vipcharge() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Vipcharge(int phone, double amt, Date amttime) {
		super();
		this.phone = phone;
		this.amt = amt;
		this.amttime = amttime;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public Date getAmttime() {
		return amttime;
	}
	public void setAmttime(Date amttime) {
		this.amttime = amttime;
	}
	
}
