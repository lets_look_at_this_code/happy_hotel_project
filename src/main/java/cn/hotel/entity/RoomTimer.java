package cn.hotel.entity;

public class RoomTimer {
	private int roomNo;
	private int inTime;
	private int outTime;
	public RoomTimer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RoomTimer(int roomNo, int inTime, int outTime) {
		super();
		this.roomNo = roomNo;
		this.inTime = inTime;
		this.outTime = outTime;
	}
	public int getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}
	public int getInTime() {
		return inTime;
	}
	public void setInTime(int inTime) {
		this.inTime = inTime;
	}
	public int getOutTime() {
		return outTime;
	}
	public void setOutTime(int outTime) {
		this.outTime = outTime;
	}
	
}
