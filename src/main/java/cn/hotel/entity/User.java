package cn.hotel.entity;

public class User {

	private int userId;
	private int permId;
	private String userName;
	private String password;
	private int staffId;
	public User() {
		super();
	}
	
	public User(int userId) {
		super();
		this.userId = userId;
	}

	public User(int userId, int permId, String userName, String password,
			int staffId) {
		super();
		this.userId = userId;
		this.permId = permId;
		this.userName = userName;
		this.password = password;
		this.staffId = staffId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPermId() {
		return permId;
	}
	public void setPermId(int permId) {
		this.permId = permId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", permId=" + permId + ", userName="
				+ userName + ", password=" + password + ", staffId=" + staffId
				+ "]";
	}
	
}
