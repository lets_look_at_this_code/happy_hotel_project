package cn.hotel.entity;

public class GoodsType {
	private int typeId;//商品类型ID
	private String category;//商品类型
	
	public GoodsType() {
		super();
	}
	
	public GoodsType(int typeId, String category) {
		super();
		this.typeId = typeId;
		this.category = category;
	}
	
	
	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}


	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return "GoodsType [typeId=" + typeId + ", category=" + category + "]";
	}

}
