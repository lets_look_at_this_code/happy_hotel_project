package cn.hotel.entity;

public class Room {
	private int roomNo;//房间号
	private int typeId;//类型Id
	private int state;//状态(可用 0,已入住1,正在打扫2,禁用3)
	private String ramark;//备注
	public int getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getRamark() {
		return ramark;
	}
	public void setRamark(String ramark) {
		this.ramark = ramark;
	}
	public Room(int roomNo, int typeId, int state, String ramark) {
		super();
		this.roomNo = roomNo;
		this.typeId = typeId;
		this.state = state;
		this.ramark = ramark;
	} 
	
}
