package cn.hotel.entity;

import java.util.Date;

public class BookRoom {
	private int billNo;//预定编号
	private String clientName;//客户姓名
	private int typeId;//房间类型编号
	private int roomNo;//房间编号
	private String notes;//客人留言
	private Date inTime;//入住时间
	private int days;//入住天数
	private Date outTime;//退房时间
	private double needPay;//需交房费
	private double realPay;//已交房费
	private double returnPay;//退换房费
	private int staffId;//操作员id
	private int state;//此单号当前状态代号  0:预定未入住,1:预定已入住,2:预定已撤销
	private int isVip;//是否是VIP 0:否,1:是
	public int getBillNo() {
		return billNo;
	}
	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Date getInTime() {
		return inTime;
	}
	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public Date getOutTime() {
		return outTime;
	}
	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}
	public double getNeedPay() {
		return needPay;
	}
	public void setNeedPay(double needPay) {
		this.needPay = needPay;
	}
	public double getRealPay() {
		return realPay;
	}
	public void setRealPay(double realPay) {
		this.realPay = realPay;
	}
	public double getReturnPay() {
		return returnPay;
	}
	public void setReturnPay(double returnPay) {
		this.returnPay = returnPay;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getIsVip() {
		return isVip;
	}
	public void setIsVip(int isVip) {
		this.isVip = isVip;
	}
	public BookRoom() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BookRoom(int billNo, String clientName, int typeId, int roomNo,
			String notes, Date inTime, int days, Date outTime, double needPay,
			double realPay, double returnPay, int staffId, int state, int isVip) {
		super();
		this.billNo = billNo;
		this.clientName = clientName;
		this.typeId = typeId;
		this.roomNo = roomNo;
		this.notes = notes;
		this.inTime = inTime;
		this.days = days;
		this.outTime = outTime;
		this.needPay = needPay;
		this.realPay = realPay;
		this.returnPay = returnPay;
		this.staffId = staffId;
		this.state = state;
		this.isVip = isVip;
	}
	

}
