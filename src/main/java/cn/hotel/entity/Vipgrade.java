package cn.hotel.entity;
//会员等级表
public class Vipgrade {
	private int gradeid;//会员等级ID
	private String geade;//会员等级
	private double countamt;//累计充值
	private double discount;//打折力度
	
	public Vipgrade() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Vipgrade(int gradeid, String geade, double countamt, double discount) {
		super();
		this.gradeid = gradeid;
		this.geade = geade;
		this.countamt = countamt;
		this.discount = discount;
	}
	public int getGradeid() {
		return gradeid;
	}
	public void setGradeid(int gradeid) {
		this.gradeid = gradeid;
	}
	public String getGeade() {
		return geade;
	}
	public void setGeade(String geade) {
		this.geade = geade;
	}
	public double getCountamt() {
		return countamt;
	}
	public void setCountamt(double countamt) {
		this.countamt = countamt;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
}
