package cn.hotel.entity;

public class Inventory {
	private int goodsId;
	private int num;
	
	
	public Inventory() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Inventory(int goodsId, int num) {
		super();
		this.goodsId = goodsId;
		this.num = num;
	}
	public int getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	@Override
	public String toString() {
		return "Inventory [goodsId=" + goodsId + ", num=" + num + "]";
	}
}
