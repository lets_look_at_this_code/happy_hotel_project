package cn.hotel.entity;

public class PurorderDt {
	private int billNo;
	private int goodsId;
	private float cost;
	private int num;
	private float countAmt;
	
	
	public PurorderDt() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PurorderDt(int billNo, int goodsId, float cost, int num,
			float countAmt) {
		super();
		this.billNo = billNo;
		this.goodsId = goodsId;
		this.cost = cost;
		this.num = num;
		this.countAmt = countAmt;
	}

	public int getBillNo() {
		return billNo;
	}
	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}
	public int getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public float getCountAmt() {
		return countAmt;
	}
	public void setCountAmt(float countAmt) {
		this.countAmt = countAmt;
	}

	@Override
	public String toString() {
		return "PurorderDt [billNo=" + billNo + ", goodsId=" + goodsId
				+ ", cost=" + cost + ", num=" + num + ", countAmt=" + countAmt
				+ "]";
	}
}
