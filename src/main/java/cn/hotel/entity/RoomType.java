package cn.hotel.entity;

public class RoomType {
	private int typeId;//房间类型Id
	private String typeName;//房间类型名称
	private double price;//房间价格
	private String description;//设施描述
	
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public RoomType(int typeId, String typeName, double price, String description) {
		super();
		this.typeId = typeId;
		this.typeName = typeName;
		this.price = price;
		this.description = description;
	}
}
