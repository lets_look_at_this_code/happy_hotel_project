package cn.hotel.entity;
/**
 * 
 * @author 项目一组--王立申
 * 		销售清单表   实体类
 *
 */
public class SaleDt {

	private int billNo;//住宿编号
	private Goods goods;//商品id
	private int num;//销售数量
	private double price;//销售单价
	private double amount;//单品总价
	private User user;
	public SaleDt() {
		super();
	}
	public SaleDt(int billNo, Goods goods, int num, double price,
			double amount, User user) {
		super();
		this.billNo = billNo;
		this.goods = goods;
		this.num = num;
		this.price = price;
		this.amount = amount;
		this.user = user;
	}
	public int getBillNo() {
		return billNo;
	}
	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "SaleDt [billNo=" + billNo + ", goods=" + goods + ", num=" + num
				+ ", price=" + price + ", amount=" + amount + ", user=" + user
				+ "]";
	}
	
}
