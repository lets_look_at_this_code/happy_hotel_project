package cn.hotel.entity;

import java.sql.Date;

public class PurorderMt {
	private int billNo;
	private float amount;
	private int userId;
	private int staffId;
	private Date date;
	
	
	public PurorderMt() {
		super();
		// TODO Auto-generated constructor stub
	}


	public PurorderMt(int billNo, float amount, int userId, int staffId,
			Date date) {
		super();
		this.billNo = billNo;
		this.amount = amount;
		this.userId = userId;
		this.staffId = staffId;
		this.date = date;
	}


	@Override
	public String toString() {
		return "PurorderMt [billNo=" + billNo + ", amount=" + amount
				+ ", userId=" + userId + ", staffId=" + staffId + ", date="
				+ date + "]";
	}
	
	 
}
