package cn.hotel.entity;

public class Permission {
	private int permId;
	private String name;
	
	public Permission() {
		super();
	}
	public Permission(int permId, String name) {
		super();
		this.permId = permId;
		this.name = name;
	}
	public int getPermId() {
		return permId;
	}
	public void setPermId(int permId) {
		this.permId = permId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
