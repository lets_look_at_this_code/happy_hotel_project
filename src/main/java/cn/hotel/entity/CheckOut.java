package cn.hotel.entity;

import java.util.Date;

/**
 * 退房结算表实体类
 * @author 许立宏
 *
 */
public class CheckOut {
	private int BillNo;//住宿登记编号
	private int roomNo;//房间号
	private double price;//房价
	private int isVip;//是否会员
	private Date inTime;//入住时间
	private Date outTime;//退房时间
	private int days;//住宿天数
	private int outDeposit;//退还押金
	private double billAmount;//账单总金额
	private int state;//订单状态 (未退房0，已退房1）
	private int userId;//用户ID
	
	public CheckOut() {
		super();
	}

	public CheckOut(int billNo, int roomNo, double price, int isVip,
			Date inTime, Date outTime, int days, int outDeposit,
			double billAmount, int state, int userId) {
		super();
		BillNo = billNo;
		this.roomNo = roomNo;
		this.price = price;
		this.isVip = isVip;
		this.inTime = inTime;
		this.outTime = outTime;
		this.days = days;
		this.outDeposit = outDeposit;
		this.billAmount = billAmount;
		this.state = state;
		this.userId = userId;
	}

	public int getBillNo() {
		return BillNo;
	}

	public void setBillNo(int billNo) {
		BillNo = billNo;
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getIsVip() {
		return isVip;
	}

	public void setIsVip(int isVip) {
		this.isVip = isVip;
	}

	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getOutDeposit() {
		return outDeposit;
	}

	public void setOutDeposit(int outDeposit) {
		this.outDeposit = outDeposit;
	}

	public double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "CheckOut [BillNo=" + BillNo + ", roomNo=" + roomNo + ", price="
				+ price + ", isVip=" + isVip + ", inTime=" + inTime
				+ ", outTime=" + outTime + ", days=" + days + ", outDeposit="
				+ outDeposit + ", billAmount=" + billAmount + ", state="
				+ state + ", userId=" + userId + "]";
	}
	

}