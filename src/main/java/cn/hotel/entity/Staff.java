package cn.hotel.entity;

public class Staff {
	private int staffId;
	private String staffName;
	private Department department;
	private double salary;
	public Staff(){
		
	}
	public Staff( String staffName,Department department, double salary) {
		this.staffName = staffName;
		this.department = department;
		this.salary = salary;
	}
	public Staff(int staffId, String staffName,Department department,double salary) {
		this.staffId = staffId;
		this.staffName = staffName;
		this.department = department;
		this.salary = salary;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
}
