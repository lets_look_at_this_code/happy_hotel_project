package cn.hotel.entity;
//会员信息表
public class Vip {
	private String phone;//会员手机号
	private String vipName;//会员姓名
	private String sex;//性别
	private String identity;//身份证号
	private double countAmt;//累计充值
	private double balance;//余额
	private int gradeId;//会员等级ID
	
	public Vip() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vip(String phone, String vipName, String sex, String identity,
			double countAmt, double balance, int gradeId) {
		super();
		this.phone = phone;
		this.vipName = vipName;
		this.sex = sex;
		this.identity = identity;
		this.countAmt = countAmt;
		this.balance = balance;
		this.gradeId = gradeId;
	}

	public Vip(String phone, double countAmt, int gradeId) {
		super();
		this.phone = phone;
		this.countAmt = countAmt;
		this.gradeId = gradeId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getVipName() {
		return vipName;
	}

	public void setVipName(String vipName) {
		this.vipName = vipName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public double getCountAmt() {
		return countAmt;
	}

	public void setCountAmt(double countAmt) {
		this.countAmt = countAmt;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getGradeId() {
		return gradeId;
	}

	public void setGradeId(int gradeId) {
		this.gradeId = gradeId;
	}

	@Override
	public String toString() {
		return "Vip [phone=" + phone + ", vipName=" + vipName + ", sex=" + sex
				+ ", identity=" + identity + ", countAmt=" + countAmt
				+ ", balance=" + balance + ", gradeId=" + gradeId + "]";
	}
	
}
