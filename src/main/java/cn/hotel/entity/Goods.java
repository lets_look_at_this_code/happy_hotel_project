package cn.hotel.entity;


public class Goods {
	private int goodsId;//商品Id
	private int typeId;//商品类型id
	private String goodsName;//商品名称
	private double price;//商品售价
	private int minNumber;//商品库存下限
	private String remarl;//商品备注
	
	public Goods() {
		super();
	}
	
	public Goods(int goodsId, int typeId, String goodsName, double price,
			int minNumber, String remarl) {
		super();
		this.goodsId = goodsId;
		this.typeId = typeId;
		this.goodsName = goodsName;
		this.price = price;
		this.minNumber = minNumber;
		this.remarl = remarl;
	}
	


	public int getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getMinNumber() {
		return minNumber;
	}
	public void setMinNumber(int minNumber) {
		this.minNumber = minNumber;
	}
	public String getRemarl() {
		return remarl;
	}
	public void setRemarl(String remarl) {
		this.remarl = remarl;
	}
	@Override
	public String toString() {
		return "Goods [goodsId=" + goodsId + ", typeId=" + typeId
				+ ", goodsName=" + goodsName + ", price=" + price
				+ ", minNumber=" + minNumber + ", remarl=" + remarl + "]";
	}

}
