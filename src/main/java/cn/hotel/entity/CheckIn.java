package cn.hotel.entity;

import java.util.Date;

/**
 * 入住登记表实体类
 * @author 许立宏
 *
 */
public class CheckIn {
	private int billNo;//住宿登记编号
	private String clientName;//顾客姓名
	private int sex;//性别(男1，女0)
	private String identity;//身份证
	private String address;//地址
	private String phone;//手机号
	private int isVip;//是否会员
	private int roomNo;//房间号
	private double price;//价格
	private Date inTime;//入住时间
	private int days;//入住天数
	private Date outTime;//预计退房时间
	private int deposit;//押金
	private int userId;//用户ID
	private String notes;//顾客留言
	private int state;//单据状态（未退房0  已退房1）
		
	public CheckIn() {
		super();
	}

	public CheckIn(int billNo, String clientName, int sex, String identity,
			String address, String phone, int isVip, int roomNo, double price,
			Date inTime, int days, Date outTime, int deposit, int userId,
			String notes, int state) {
		super();
		this.billNo = billNo;
		this.clientName = clientName;
		this.sex = sex;
		this.identity = identity;
		this.address = address;
		this.phone = phone;
		this.isVip = isVip;
		this.roomNo = roomNo;
		this.price = price;
		this.inTime = inTime;
		this.days = days;
		this.outTime = outTime;
		this.deposit = deposit;
		this.userId = userId;
		this.notes = notes;
		this.state = state;
	}

	public int getBillNo() {
		return billNo;
	}

	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getIsVip() {
		return isVip;
	}

	public void setIsVip(int isVip) {
		this.isVip = isVip;
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CheckIn [billNo=" + billNo + ", clientName=" + clientName
				+ ", sex=" + sex + ", identity=" + identity + ", address="
				+ address + ", phone=" + phone + ", isVip=" + isVip
				+ ", roomNo=" + roomNo + ", price=" + price + ", inTime="
				+ inTime + ", days=" + days + ", outTime=" + outTime
				+ ", deposit=" + deposit + ", userId=" + userId + ", notes="
				+ notes + ", state=" + state + "]";
	}

	

	

}
