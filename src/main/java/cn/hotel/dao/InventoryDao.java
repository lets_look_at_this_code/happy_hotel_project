package cn.hotel.dao;

import cn.hotel.entity.Inventory;

/**
 * 对库存表的操作
 * 
 * @author 沈运广
 *
 */
public interface InventoryDao {

	/**
	 * 向仓库中添加商品
	 * 
	 * @param goods
	 * @return
	 */
	public int addInventory(Inventory inven);

	/**
	 * 删除库存表中已存在的商品
	 * 
	 * @param goods
	 * @return
	 */
	public int deleteInventory(Inventory inven);

	/**
	 * 修改库存表
	 * 
	 * @param goods
	 * @return
	 */
	public int updateInventory(Inventory inven);
	
	/**
	 * 根据商品Id查找库存中是否已经存在该商品
	 * @param goodsId
	 * @return
	 */
	public Inventory searchInventory(int goodsId);
}
