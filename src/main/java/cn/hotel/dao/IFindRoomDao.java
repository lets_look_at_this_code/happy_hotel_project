package cn.hotel.dao;

import java.util.List;

import cn.hotel.entity.RoomTimer;

public interface IFindRoomDao {
	public int[] notBadRoom();
	public List<RoomTimer> existRoom();
}
