package cn.hotel.dao;

import java.util.List;
import java.util.Map;

import cn.hotel.entity.Staff;

public interface StaffDao {
	List<Staff> getStaff(Map<String,Integer> map);
	int getNum(String searchs);
	int upDateStaff(Staff staff);
	int deleteStaff(int staffId);
	int addStaff(Staff staff);
}
