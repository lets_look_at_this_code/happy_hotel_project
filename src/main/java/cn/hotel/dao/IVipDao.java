package cn.hotel.dao;

import cn.hotel.entity.Vip;

/**
 * 会员信息操作层
 * @author 张瑞峰
 *
 */

public interface IVipDao {
	/**
	 * 向表中插入会员数据
	 * @param vip 会员信息表对象
	 * @return 返回受影响行数
	 */
	public int addVip (Vip vip);
	
	/**
	 * 根据手机号删除会员信息
	 * @param phone 手机号
	 * @return 返回受影响行数
	 */
	public int deleteVip(String phone);
	
	/**
	 * 修改会员信息
	 * @param vip 会员信息表对象
	 * @return 返回受影响行数
	 */
	public int updateVip(Vip vip);
	
	/**
	 * 查询会员信息
	 * @param phone 根据手机号查询会员信息
	 * @return 返回受影响行数
	 */
	public Vip selectVip(String phone);
}
