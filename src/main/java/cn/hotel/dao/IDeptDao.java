package cn.hotel.dao;

import java.util.List;
import cn.hotel.entity.Department;

public interface IDeptDao {
	List<Department> getDept();
	int deleteDepartment(int deptId);
	int updateDepartment(Department department);
	int addDepartment(Department department);
}
