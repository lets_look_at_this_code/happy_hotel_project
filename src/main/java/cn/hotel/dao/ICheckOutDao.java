package cn.hotel.dao;

import cn.hotel.entity.CheckOut;

/**
 * 退房结算数据操作层
 * @author 许立宏
 *
 */
public interface ICheckOutDao {
	
	/**
	 * 向退房结算表中插入单据
	 * @param checkOut 退房结算单据对象
	 * @return 返回影响行数
	 */
	public int addCheckOutBill(CheckOut checkOut);
	
	/**
	 * 修改房间
	 * @param checkOut 退房结算单据对象
	 * @return 返回影响行数
	 */
	public int updateRoomNo(CheckOut checkOut);
	
	/**
	 * 修改入住天数
	 * @param checkOut
	 * @return
	 */
	public int updateDays(CheckOut checkOut);
	
	/**
	 * 退房结算
	 * @param checkOut
	 * @return
	 */
	public int updateCheckOut(CheckOut checkOut);
}
