package cn.hotel.dao;


import java.util.List;

import cn.hotel.entity.Goods;
/**
 * 对商品表信息进行增删改查
 * @author wlsh
 *
 */
public interface IGoodsDao {
	
	
	/**
	 * 通过商品名称得到商品信息
	 * @param goodsName 商品名称
	 * @return 一个商品对象
	 */
	public Goods getGoodsByName(String goodsName);
	
	
	/**
	 * 给商品表插入信息
	 * @param goods 一条商品信息
	 * @return int
	 */
	public int addGoods(Goods goods);
	
	
	/**
	 * 通过商品ID删除商品
	 * @param goodsId 商品id
	 * @return int
	 */
	public int deleteGoods(int goodsId);
	
	
	/**
	 * 按照商品ID修改商品信息
	 * @param goods 一条商品信息
	 * @return int
	 */
	public int updateGoods(Goods goods);
	
	/**
	 * 查询所有的商品信息
	 * @return
	 */
	public List<Goods> getAllGoods();

	/**
	 * 通过商品类型查询商品列表
	 * @param typeId
	 * @return
	 */
	public List<Goods> getGoodsById(int typeId);
	
}