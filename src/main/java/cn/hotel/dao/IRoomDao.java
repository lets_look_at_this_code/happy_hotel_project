package cn.hotel.dao;

import java.util.List;

import cn.hotel.entity.Room;

public interface IRoomDao {
	/**
	 * 添加房间信息
	 * @param room
	 * @return
	 */
	int addRoom(Room room);
	
	/**
	 * 根据房间号删除房间信息
	 * @param roomNo
	 * @return int
	 */
	int deleteRoom(int roomNo);
	
	/**
	 * 通过房间号 查询房间信息
	 * @param int
	 * @return room
	 */
	Room queryRoom(int roomNo);
	
	
	/**
	 * 通过房间类型Id 查询房间信息
	 * @param typeId
	 * @return room
	 */
	List<Room> getRoomByTypeId(int typeId);
	
	/**
	 * 修改房间信息	
	 * @param room
	 * @return int
	 */
	int alterRoom(Room room);

}
