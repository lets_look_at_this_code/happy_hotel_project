package cn.hotel.dao;

import java.util.List;
import java.util.Map;

import cn.hotel.entity.CheckIn;

/**
 * 入住登记数据操作层
 * @author 许立宏
 *
 */
public interface ICheckInDao {
	
	/**
	 * 向入住登记表中插入单据
	 * @param checkIn 入住登记对象
	 * @return 返回影响行数
	 */
	public int addCheckInBill(CheckIn checkIn);
	
	/**
	 * 根据单据编号修改入住登记表的房间号
	 * @return 返回影响行数
	 */
	public int updateCheckInBill(CheckIn checkIn);
	
	/**
	 * 获取入住登记表的最大单据编号
	 * @return
	 */
	public int getMaxBillNo();
	
	/**
	 * 根据条件（单据状态、起止时间、房间号）查询入住登记单据
	 * @param map
	 * @return
	 */
	public List<CheckIn> selectCheckInBill(Map<String, Object> map);
	
	/**
	 * 根据条件查询总单据数
	 * @param map
	 * @return
	 */
	public int selectBillCount(Map<String, Object> map);
}
