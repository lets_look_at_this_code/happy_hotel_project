package cn.hotel.dao;

import java.util.List;

import cn.hotel.entity.RoomType;

/**
 * 对房间类型表的操作接口
 * @author 朱康康
 *
 */
public interface IRoomTypeDao {

	/**
	 * 增加房间类型	
	 * @param RoomType rt
	 * @return int
	 */
	public int addRoomtype(RoomType rt);
	/**
	 * 删除房间类型	
	 * @param RoomType rt
	 * @return int
	 */
	public int deleteRoomtype(int typeId);
	/**
	 * 查询所有房间类型	
	 * 无条件查询
	 * @param RoomType rt
	 * @return List
	 */
	public List<RoomType> queryRoomtype();
	
	/**
	 * 根据房间类型查询房间价格
	 * @param int 房间类型Id
	 * @return RoomType 房间类型信息
	 */
	public RoomType queryRoomtype(int typeId);
	/**
	 * 修改房间类型
	 * @param RoomType rt
	 * @return int
	 */
	public int alterRoomtype(RoomType rt);
}
