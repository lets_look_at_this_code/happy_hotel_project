package cn.hotel.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 日期转换工具类
 * @author 许立宏
 *
 */
public class TransformDateUtil {
	
	/**
	 * "yyyy-MM-dd"类型字符串转换为java.sql.Date
	 * @param date
	 * @return
	 */
	public static Date stringToSqlDate(String date){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		if(date == null){
			return null;
		}
		try {
			long date1 = df.parse(date).getTime();
			Date sqlDate = new Date(date1);
			return sqlDate;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
