package cn.hotel.util;

public class PageUtil{
	public static int getTotalPages(int pageSize,int pageNum){
		int totalPages = (pageNum%pageSize == 0)?(pageNum/pageSize):(pageNum/pageSize+1);
		return totalPages;
	}
}	
