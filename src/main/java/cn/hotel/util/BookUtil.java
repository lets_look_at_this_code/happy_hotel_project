package cn.hotel.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.hotel.entity.RoomTimer;

public class BookUtil {
	/*
	 * 获得日期参数与今天零点的小时差值
	 */
	public static int getHour(Date date) throws ParseException{
		Date date1 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String sdate1 = sdf.format(date1);
		Date date2 = sdf.parse(sdate1);
		long ldate = date.getTime();
		long ldate2 = date2.getTime();
		int hours = (int)Math.round((ldate-ldate2)/(60*60*1000));
		return hours;
	}
	
//	冒泡法升序排列List集合
	public static void maoPao(List<Integer> list){
		for (int i = 0; i < list.size()-1; i++) {
			for (int j = 0; j < list.size()-i-1; j++) {
				if (list.get(j)>list.get(j+1)) {
					int a=list.get(j);
					list.set(j,list.get(j+1));
					list.set(j+1, a);
				}
			}
		}
	}
	/*将住房登记表(已入住状态)和预定表(预定未入住)所有的房间号,开始时间,
	 * 退房时间作为一个类封装在RoomTimer类中,然后将每一个对象存放到List集合中
	 * 将List中的房间号作为键,该房间号对应的所有起止时间(以某个时间点计算小时差)
	 * 作为值放入map集合中
	 */
	/**
	 * 
	 * @param list 
	 * @return
	 */
	public static Map<Integer,List<Integer>> testMap(List<RoomTimer> list){
		
		Map<Integer,List<Integer>> map = new HashMap<Integer,List<Integer>>();
		for(RoomTimer roomtimer:list){
			List<Integer> lists = new ArrayList<Integer>();
			lists.add(roomtimer.getInTime());
			lists.add(roomtimer.getOutTime());
			if(map.containsKey(roomtimer.getRoomNo())){
				List<Integer> plist = map.get(roomtimer.getRoomNo());
				plist.addAll(lists);
				lists = plist;
			}
			maoPao(lists);
			map.put(roomtimer.getRoomNo(), lists);
		}
		return map;
	}
//	判断所有已入住和已预订的房间是否可供再次入住或预定,如果有则返回该房间号
	public static Integer existRoom(List<RoomTimer> list,int intime,int outtime){
		Map<Integer,List<Integer>> map = testMap(list);
		Set<Map.Entry<Integer, List<Integer>>> set = map.entrySet();
		for(Map.Entry<Integer, List<Integer>> mapentry : set){
			List<Integer> listint = mapentry.getValue();
			for(int i = 1; i < listint.size();i= i+2){
				if(i!=listint.size()-1){
					if(listint.get(i)<intime && listint.get(i+1)>outtime){
						return mapentry.getKey();
					}
				}else{
					if(listint.get(i)<intime){
						return mapentry.getKey();
					}
				}
			  	
			}
		}
		return null;
	}
//	当所有已入住和有预定的房间都不可以入住或者预定时查询其他空房间,如果有就返回该房间号
	 public static Integer existRooms(int[] itg,List<RoomTimer> list){
		 Set<Integer> setint = testMap(list).keySet();
		 for (int i = 0; i < itg.length; i++) {
			if(!setint.contains(itg[i])){
				return itg[i];
			}
		}
		 return null;
	 }
}
