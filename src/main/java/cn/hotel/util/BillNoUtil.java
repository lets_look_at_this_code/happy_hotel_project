package cn.hotel.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 按日期和三位编号生成单据编号
 * @author 许立宏
 *
 */
public class BillNoUtil {
	public static String getBillNo(String maxBillno){
		String oldBillNo = maxBillno.substring(0,6);
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		String billNo = dateFormat.format(date);
		if(oldBillNo.equals(billNo)){
			billNo = null;
		}else{
			billNo = billNo+"001";
		}
		return billNo;
	}
}

