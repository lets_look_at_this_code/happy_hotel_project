package cn.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hotel.busi.ICheckOutBusi;
import cn.hotel.busi.impl.CheckOutBusiImpl;
import cn.hotel.entity.CheckIn;
import cn.hotel.entity.CheckOut;

/**
 * 退房结算Servlet层
 * @author 许立宏
 *
 */
@WebServlet("/CheckOutServlet")
public class CheckOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckOutServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if("addBill".equals(action)){
			addCheckOutBill(request, response);
		}
	}

	//插入退房结算表单据
	public void addCheckOutBill(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		CheckIn checkIn = (CheckIn)request.getAttribute("checkIn");
		CheckOut checkOut = new CheckOut();
		checkOut.setBillNo(checkIn.getBillNo());
		checkOut.setRoomNo(checkIn.getRoomNo());
		checkOut.setPrice(checkIn.getPrice());
		checkOut.setIsVip(checkIn.getIsVip());
		checkOut.setInTime(checkIn.getInTime());
		checkOut.setDays(checkIn.getDays());
		checkOut.setBillAmount(checkIn.getPrice()*checkIn.getDays());
		checkOut.setState(0);
		ICheckOutBusi checkOutBusi = new CheckOutBusiImpl();
		boolean result = checkOutBusi.addCheckOut(checkOut);
		if(result){
			response.sendRedirect("CheckInBill.jsp");
		}else{
			PrintWriter out = response.getWriter();
			out.print("<script>alert('退房结算单据未生成！请及时联系统管理员！');</script>");
			out.flush();
			out.close();
		}
	}

}
