package cn.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hotel.busi.IGoodsTypeBusi;
import cn.hotel.busi.impl.GoodsTypeBusiImpl;
import cn.hotel.entity.GoodsType;

/**
 * Servlet implementation class GoodsTypeServlet
 */
@WebServlet("/GoodsTypeServlet")
public class GoodsTypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsTypeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IGoodsTypeBusi gtb = new GoodsTypeBusiImpl();
		String action = request.getParameter("action");
		PrintWriter out = response.getWriter();
		if("goodsTypeAjax".equals(action)){
			List<GoodsType> list = gtb.getGoodsType();
			String date = "";
			String goodsTypeId = "";
			String goodsTypeName = "";
			for (GoodsType goodsType : list) {
				goodsTypeId += goodsType.getTypeId()+":";
				goodsTypeName += goodsType.getCategory()+":";
			}
			date = goodsTypeId+"-"+goodsTypeName;
			out.print(date);
			out.flush();
			out.close();
		}
	}

}
