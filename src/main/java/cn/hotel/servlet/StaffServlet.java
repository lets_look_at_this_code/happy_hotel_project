package cn.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hotel.busi.IDeptBusi;
import cn.hotel.busi.StaffBusi;
import cn.hotel.busi.impl.IDeptBusiImpl;
import cn.hotel.busi.impl.StaffBusiImpl;
import cn.hotel.entity.Department;
import cn.hotel.entity.Staff;
/**
 * Servlet implementation class Servlet
 * 
 */
@WebServlet("/StaffServlet")
public class StaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaffServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		if("addStaff".equals(request.getParameter("operation")) & "updateStaffSubmit".equals(action)){
			//当action为addStaff和操作为添加时跳转Add方法
			this.Add(request, response);
			return;
		}else if("updateStaff".equals(action) | "addStaff".equals(action)){
			//当action为updateStaff时跳转修改员工方法
			this.updateStaff(request,response);
			return;
		}else if("updateStaffSubmit".equals(action) | "updateStaff".equals(request.getParameter("operation"))){
			//当action为updateStaffSubmit和操作为修改时跳转修改方法
			this.updateStaff_Submit(request, response);
			return;
		}else if("deleteStaff".equals(action)){
			//当action为deleteStaff时跳转删除方法
			this.Delete(request, response);
			return;
		}else if("show".equals(action)){
			//当action为show是跳转分页展示并且带搜索的方法
			this.Show(request, response);
			return;
		}else if("deleteDepartment".equals(action)){
			//当action为deleteDepartment时跳转删除部门方法
			this.deleteDepartment(request, response);
			return;
		}else if("updateDepartment".equals(action)){
			//当action为updateDepartment时跳转修改部门方法
			this.updateDepartment(request, response);
			return;
		}else if("addDepartment".equals(action)){
			//当action为addDepartment时跳转添加部门方法
			this.addDepartment(request, response);
			return;
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}
	protected void updateStaff(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		int staffId = 0;
		if(request.getParameter("id") != null){
			staffId = Integer.parseInt(request.getParameter("id"));
		}
		//当ID不等于null时获得ID
		String operation = request.getParameter("operation");
		String str = "";
		if("addStaff".equals(operation)){
		//判断操作为修改员工还是添加员工
			str = "添加";
		}else{
			str = "修改";
		}
		IDeptBusi dept = new IDeptBusiImpl();
		//创建Busi层接口对象
		List<Department> list = dept.getDept();
		//获得部门集合
		request.setAttribute("list",list);
		//跳转页面
		request.getRequestDispatcher("updateStaff.jsp?id="+staffId+"&operation="+operation+"&str="+str).forward(request, response);;
	}
	protected void updateStaff_Submit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		int deptId = Integer.parseInt(request.getParameter("dept"));
		//获得部门ID
		String staffName = request.getParameter("name");
		//获得部门名称
		int staffId = Integer.parseInt(request.getParameter("id"));
		//获得员工ID
		double salary = Double.parseDouble(request.getParameter("salary"));
		//获得员工工资
		StaffBusi staffbusi = new StaffBusiImpl();
		//创建StaffBusi层接口对象
		staffbusi.upDateStaff(staffId,staffName,deptId, salary);
		//修改数据库
	}
	protected void Delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//获得输出流
		PrintWriter out = response.getWriter();
		//获得员工ID
		int staffId = Integer.parseInt(request.getParameter("id"));
		//创建StaffBusi层接口对象
		StaffBusi staff = new StaffBusiImpl();
		//判断是否删除成功
		if(staff.deleteStaff(staffId)){
			out.print("删除成功");
		}else{
			out.print("删除失败");
		}
		//关闭输出流
		out.close();
	}
	protected void Show(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//获得StaffBusi层接口对象
		StaffBusi staff = new StaffBusiImpl();
		//获得当前页数
		String str = request.getParameter("pageIndex");
		//获得要搜索的值
		String searchs = request.getParameter("searchs");
		//判断是否为空
		String search = null;
		if("".equals(searchs)){
		}else{
			if(searchs != null){
				search = "%"+searchs+"%";
			}
		}
		if("".equals(searchs)){
			searchs = null;
		}
		if(str == null){
			str = "1";
		}
		int pageIndex = Integer.parseInt(str);
		//设置每页显示条数
		System.out.println(searchs);
		System.out.println(pageIndex);
		int pageSize = 4;
		//获得总条数
		int pageNum = staff.getStaffNum(search);
		System.out.println(pageNum);
		//获得总页数
		int totalPages = cn.hotel.util.PageUtil.getTotalPages(pageSize, pageNum);
		//判断
		if(pageIndex < 1){
			//如果小于1当前页数等于1
			pageIndex = 1;
		}
		if(pageIndex > totalPages){
			//如果大于总页数当前页数等于总页数
			pageIndex = totalPages;
		}
		//获得员工集合
		List<Staff> list= staff.getStaff((pageIndex - 1) * pageSize,pageSize,search);
		//创建对象
		IDeptBusi dept = new IDeptBusiImpl();
		//获得部门集合
		List<Department> deptlist = dept.getDept();
		//存入集合
		request.setAttribute("deptlist",deptlist);
		//存入当前页数
		request.setAttribute("pageIndex",pageIndex);
		//存入总页数
		request.setAttribute("totalPages",totalPages);
		//存入总条数
		request.setAttribute("num",pageNum);
		//存入集合
		request.setAttribute("list",list);
		//存入搜索
		request.setAttribute("searchs",searchs);
		//跳转页面
		request.getRequestDispatcher("staff.jsp").forward(request,response);
	}
	protected void Add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//获得部门ID
		int deptId = Integer.parseInt(request.getParameter("dept"));
		//获得部门名称
		String staffName = request.getParameter("name");
		//获得员工工资
		double salary = Double.parseDouble(request.getParameter("salary"));
		//创建对象
		StaffBusi staffbusi = new StaffBusiImpl();
		//创建参数对象
		Staff staff = new Staff(staffName,new Department(deptId),salary);
		//传参
		staffbusi.addStaff(staff);
	}
	protected void deleteDepartment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//创建输出流
		PrintWriter out = response.getWriter();
		//获得部门ID
		int deptId = Integer.parseInt(request.getParameter("deptid"));
		//创建对象
		IDeptBusi dept = new IDeptBusiImpl();
		//判断是否删除成功
		if(dept.deleteDepartment(deptId)){
			out.print("删除成功");
		}else{
			out.print("删除失败");
		}
	}
	protected void updateDepartment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获得部门ID
		int deptId = Integer.parseInt(request.getParameter("deptid"));
		//获得部门名称
		String deptName = request.getParameter("deptname");
		//创建对象
		IDeptBusi dept = new IDeptBusiImpl();
		//传参
		dept.updateDepartment(deptId,deptName);
	}	
	protected void addDepartment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获得部门名称
		String deptName = request.getParameter("deptname");
		//创建部门对象
		Department department = new Department(deptName);
		IDeptBusi dept = new IDeptBusiImpl();
		//传参
		dept.addDepartment(department);
	}	
}
