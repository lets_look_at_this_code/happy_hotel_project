package cn.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.hotel.busi.ISaleDtBusi;
import cn.hotel.busi.impl.SaleDtBusiImpl;
import cn.hotel.entity.SaleDt;

/**
 * Servlet implementation class SaleDtServlet
 */
@WebServlet("/SaleDtServlet")
public class SaleDtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaleDtServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		/*
		 * 判断参数进入不同分支，对数据库进行不同操作
		 */
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		ISaleDtBusi sdb = new SaleDtBusiImpl();
		if ("add".equals(action)) {
			int billNo = (Integer)session.getAttribute("billNo");
			String goodsName = request.getParameter("goodsName");
			int num = Integer.parseInt(request.getParameter("num"));
			//int userId = (Integer)session.getAttribute("userId");
			boolean result = sdb.addSaleGoods(billNo, goodsName, num, 10001);
			if(result){
				request.getRequestDispatcher("SaleDtServlet?action=select").forward(request, response);
//				response.sendRedirect("SaleDtServlet?action=select");
			}else{
				response.sendRedirect("saleDtAdd.jsp");
			}
		} else if ("delete".equals(action)) {
			int billNo = (Integer)session.getAttribute("billNo");
			int goodsId = Integer.parseInt(request.getParameter("goodsId"));
			boolean result = sdb.deleteSaleDt(billNo, goodsId);
			if(result){
				request.getRequestDispatcher("SaleDtServlet?action=select").forward(request, response);

//				response.sendRedirect("SaleDtServlet?action=select");
			}else{
				PrintWriter out = response.getWriter();
				out.print("<script> alert('删除失败！');location.href='SaleDtServlet?action=select';</script>");
			}
		} else if ("update".equals(action)) {

		} else if ("select".equals(action)) {
			int billNo = 0;
			try {
				if (request.getParameter("billNo") == null
						|| request.getParameter("billNo").equals("")) {
					billNo = (Integer) session.getAttribute("billNo");
				} else {
					billNo = Integer.parseInt(request.getParameter("billNo"));
					session.setAttribute("billNo", billNo);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			List<SaleDt> list = sdb.getSaleDtByBillNo(billNo);

			session.setAttribute("saleDtLists", list);

			response.sendRedirect("saleDtList.jsp");

		}

	}

}
