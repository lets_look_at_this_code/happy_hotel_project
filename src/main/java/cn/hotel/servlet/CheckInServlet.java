package cn.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.hotel.busi.ICheckInBusi;
import cn.hotel.busi.impl.CheckInBusiIMpl;
import cn.hotel.entity.CheckIn;
import cn.hotel.entity.User;
import cn.hotel.util.PageUtil;
import cn.hotel.util.TransformDateUtil;

/**
 * 入住登记Servlet层
 * @author 许立宏
 *
 */
@WebServlet("/CheckInServlet")
public class CheckInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckInServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		if("addCheckIn".equals(action)){
			addCheckInBill(request, response, session);
		}else if("showCheckIn".equals(action)){
			showCheckInBill(request, response, session);
		}
	}

	//分页显示入住登记单据
	public void showCheckInBill(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws ServletException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String state1 = request.getParameter("state");
		int state;
		if(state1 == null){
			state = 0;
		}else{
			state = Integer.parseInt(state1);
		}
		map.put("state", state);
		request.setAttribute("state", state);
		String startDate = request.getParameter("startDate");
		java.sql.Date sDate = TransformDateUtil.stringToSqlDate(startDate);
		map.put("startDate", sDate);
		request.setAttribute("startDate", startDate);
		String endDate = request.getParameter("endDate");
		java.sql.Date eDate = TransformDateUtil.stringToSqlDate(endDate);
		map.put("endDate", eDate);
		request.setAttribute("endDate", endDate);
		String roomNoS = request.getParameter("roomNo");
		int roomNo = 0;
		if(roomNoS == null){
			roomNo = 0;
		}else{
			try {
				roomNo = Integer.parseInt(roomNoS);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		map.put("roomNo", roomNo);
		request.setAttribute("roomNo", roomNo);
		ICheckInBusi checkInBusi = new CheckInBusiIMpl();
		int count = checkInBusi.selectBillCount(map);
		int pageSize = 10;
		int totalPages = PageUtil.getTotalPages(pageSize, count);
		String getPage = request.getParameter("pageIndex");
		if(getPage == null){
			getPage = "1";
		}
		int pageIndex = Integer.parseInt(getPage);		
		if(pageIndex < 1){
			pageIndex = 1;
		}else if(pageIndex > totalPages){
			pageIndex = totalPages;
		}
		if(pageSize > count-pageSize*(pageIndex-1)){
			pageSize = count-pageSize*(pageIndex-1);
		}
		int pageStart = pageSize*(pageIndex-1);
		map.put("pageStart", pageStart);
		map.put("pageSize", pageSize);
		List<CheckIn> ckeckInList = checkInBusi.selectCheckInBill(map);
		request.setAttribute("checkInList", ckeckInList);
		request.setAttribute("pageIndex", pageIndex);
		request.setAttribute("totalPages", totalPages);
		request.setAttribute("count", count);
		request.getRequestDispatcher("CheckInBill.jsp").forward(request, response);
	}

	//插入入住登记单据
	public void addCheckInBill(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, ServletException {
		CheckIn checkIn = new CheckIn();
		User user = (User)session.getAttribute("user");
		checkIn.setUserId(user.getUserId());
		checkIn.setClientName(request.getParameter("clientName"));
		checkIn.setSex(Integer.parseInt(request.getParameter("sex")));
		checkIn.setIdentity(request.getParameter("identity"));
		checkIn.setAddress(request.getParameter("address"));
		checkIn.setPhone(request.getParameter("phone"));
		checkIn.setIsVip(Integer.parseInt(request.getParameter("isVip")));
		checkIn.setRoomNo(Integer.parseInt(request.getParameter("roomNo")));
		checkIn.setPrice(Double.parseDouble(request.getParameter("price")));
		checkIn.setInTime(new java.sql.Date(new java.util.Date().getTime()));
		int days = Integer.parseInt(request.getParameter("days"));
		checkIn.setDays(days);
		Date d = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String date = df.format(new Date(d.getTime()+(long)days*24*60*60*1000));
		date = date+" 12:00:00";
		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			long time = df1.parse(date).getTime();
			checkIn.setOutTime(new java.sql.Date(time));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		checkIn.setDeposit(Integer.parseInt(request.getParameter("deposit")));
		checkIn.setNotes(request.getParameter("notes"));
		checkIn.setState(0);
		ICheckInBusi checkInBusi = new CheckInBusiIMpl();
		checkIn = checkInBusi.addCheckIn(checkIn);
		if(checkIn == null){
			PrintWriter out = response.getWriter();
			out.print("<script>alert('登记未成功！请重新登记！');localtion.href='bookRoom.jsp';</script>");
			out.flush();
			out.close();
		}
		String bookBillNo = request.getParameter("bookRoomNo");
		if(bookBillNo != null){
			int billNo = Integer.parseInt(bookBillNo);
			//修改房间预定单据状态
		}
		request.setAttribute("checkIn", checkIn);
		request.getRequestDispatcher("CheckOutServlet?action=addBill").forward(request, response);
	}

}
