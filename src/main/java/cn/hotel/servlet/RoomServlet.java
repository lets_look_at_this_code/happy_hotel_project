package cn.hotel.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.hotel.busi.impl.RoomBusiImpl;
import cn.hotel.entity.Room;

/**
 * Servlet implementation class SearchNewsServlet
 */
@WebServlet("/RoomServlet")
public class RoomServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RoomServlet() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		if ("add".equals(action)) {

			String state = request.getParameter("state");
			int roomNo = Integer.parseInt(request.getParameter("roomNo"));

		} else if ("del".equals(action)) {

		} else if ("upd".equals(action)){
			
		}else if ("sel".equals(action)){
		int typeId = Integer.parseInt(request.getParameter("typeId"));

		RoomBusiImpl rbi = new RoomBusiImpl();
		List<Room> list = rbi.getRoomByTypeId(typeId);

		HttpSession session = request.getSession();
		session.setAttribute("roomList", list);
		response.sendRedirect("RoomList.jsp");
		}
	}

}