package cn.hotel.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.hotel.busi.IGoodsBusi;
import cn.hotel.busi.impl.GoodsBusiImpl;
import cn.hotel.entity.Goods;
import cn.hotel.entity.GoodsType;

/**
 * Servlet implementation class GoodsServlet
 */
@WebServlet("/GoodsServlet")
public class GoodsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GoodsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		/*
		 * 根據傳入参数action的不同,选择不同的执行程序
		 */
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		IGoodsBusi goodsBusi = new GoodsBusiImpl();
		String action = request.getParameter("action");

		PrintWriter out = response.getWriter();
		
		//添加商品
		if ("add".equals(action)) {
			String goodsName = request.getParameter("goodsName");
			String goodsType = request.getParameter("goodsType");
			String price1 = request.getParameter("price");
			String minNumber1 = request.getParameter("minNumber");
			String remark = request.getParameter("remark");
			// 转换参数类型
			int typeId = Integer.parseInt(goodsType);
			int price = Integer.parseInt(price1);
			int minNumber = Integer.parseInt(minNumber1);
			// 返回一个Boolean类型的值并判断结果传到前台
			boolean result = goodsBusi.addGoods(typeId, goodsName, price,
					minNumber, remark);
			if (result) {
				request.getRequestDispatcher("GoodsServlet?action=sel")
						.forward(request, response);
			} else {
				response.sendRedirect("goods-add.jsp");// 重定向返回页面
			}

		//删除商品
		} else if ("del".equals(action)) {
			String goodsId1 = request.getParameter("goodsId");
			// 转换数据类型
			int goodsId = Integer.parseInt(goodsId1);
			// 返回一个Boolean类型的值
			boolean result = goodsBusi.deleteGoodsById(goodsId);
			if (result) {
				request.getRequestDispatcher("GoodsServlet?action=sel")
						.forward(request, response);
			} else {
				response.sendRedirect("good-list.jsp");
			}
		
		//修改商品
		} else if ("upd".equals(action)) {
			// 获取参数,把jsp文件中的数据读取到出来
			String goodsId1 = request.getParameter("goodsId");
			String goodsName = request.getParameter("goodsName");
			String goodsType = request.getParameter("goodsType");
			String price1 = request.getParameter("price");
			String minNumber1 = request.getParameter("minNumber");
			String remark = request.getParameter("remark");
			// 转换参数类型
			int goodsId = Integer.parseInt(goodsId1);
			int typeId = Integer.parseInt(goodsType);
			int price = Integer.parseInt(price1);
			int minNumber = Integer.parseInt(minNumber1);
			// 返回一个Boolean类型的值并判断结果传到前台
			boolean result = goodsBusi.updateGoods(goodsId, typeId, goodsName, price, minNumber, remark);
			if (result) {
				request.getRequestDispatcher("GoodsServlet?action=sel")
						.forward(request, response);
			} else {
				response.sendRedirect("goods-update.jsp");// 重定向返回页面
			}
			
		//查询全部商品
		} else if ("sel".equals(action)) {

			// 返回一个Goods类型的值
			List<Goods> result = goodsBusi.getAllGoods();
			//使用request对象的getSession()获取session
			HttpSession session = request.getSession();
			//将数据存储到session中
			session.setAttribute("goodsLists", result);
			// 重定向返回页面
			response.sendRedirect("goods-list.jsp");
			
		//查询部分商品
		}/*else if ("s".equals(action)) {
			Goods result = goodsBusi.getGoodsByName(goodsName);
			HttpSession session = request.getSession();
			session.setAttribute("goods", result);
			response.sendRedirect("goods-list.jsp");
		}*/
		else if ("ajaxSelect".equals(action)) {
			
			int typeId = Integer.parseInt(request.getParameter("typeId"));
			// 返回一个Goods类型的值
			List<Goods> list = goodsBusi.getGoodsById(typeId);
			String date = "";
			String goodsId = "";
			String goodsName = "";
			for (Goods goods : list) {
				goodsId += goods.getGoodsId()+":";
				goodsName += goods.getGoodsName()+":";
			}
			date = goodsId+"-"+goodsName;
			out.print(date);
			out.flush();
			out.close();
			
		}
		
	}

}
