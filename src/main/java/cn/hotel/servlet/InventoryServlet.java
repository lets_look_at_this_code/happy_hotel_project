package cn.hotel.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hotel.busi.InventoryBusi;
import cn.hotel.busi.impl.InventoryBusiImpl;
import cn.hotel.entity.Inventory;

public class InventoryServlet extends HttpServlet {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InventoryServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		String Id = request.getParameter("goodsId");
		int goodsId = Integer.parseInt(Id);
		String num = request.getParameter("num");
		int number = Integer.parseInt(num);
		Inventory inven = new Inventory(goodsId,number);
		InventoryBusi invenbs = new InventoryBusiImpl();
		invenbs.addInventory(inven);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
