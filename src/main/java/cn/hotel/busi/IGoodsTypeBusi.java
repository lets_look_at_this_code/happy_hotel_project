package cn.hotel.busi;

import java.util.List;

import cn.hotel.entity.GoodsType;

public interface IGoodsTypeBusi {
	/**
	 * 查询所有的商品类型信息
	 * @return
	 */
	List<GoodsType> getGoodsType();
}
