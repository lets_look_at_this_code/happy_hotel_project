package cn.hotel.busi;

import java.util.List;

import cn.hotel.entity.RoomType;

/**
 * 操控房间类型表dao层接口对数据库的更改
 * @author 朱康康
 *
 */
public interface IRoomTypeBusi {
	/**
	 * 增加房间类型
	 * @param typeId
	 * @param typeName
	 * @param price
	 * @param description
	 * @return boolean
	 */
	boolean addRoomtype(int typeId,String typeName,double price,String description);
	
	/**
	 * 删除房间类型
	 * @param typeId
	 * @return boolean
	 */
	boolean deleteRoomType(int typeId);
	
	/**
	 * 根据类型Id查看房间类型信息
	 * @param typeId
	 * @return RoomType
	 */
	RoomType queryRoomType(int typeId);
	
	/**
	 * 查询所有房间类型	
	 * 无条件查询
	 * @return List
	 */
	public List<RoomType> queryRoomtype();
	
	/**
	 * 修改房间类型表
	 * @param typeId
	 * @param typeName
	 * @param price
	 * @param description
	 * @return boolean
	 */
	boolean alterRoomtype(int typeId,String typeName,double price,String description);
	
}
