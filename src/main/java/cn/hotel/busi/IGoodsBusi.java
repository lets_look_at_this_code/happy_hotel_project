package cn.hotel.busi;


import java.util.List;

import cn.hotel.entity.Goods;

/**
 * 
 * @author 王亚峰
 *
 */
public interface IGoodsBusi {
	
	/**
	 * 添加商品信息
	 * @param Goods
	 */
	public boolean addGoods( int typeId, String goodsName,
			double price, int minNumber, String remark);
	
	/**
	 * 按照名字删除商品
	 * @param goodsName 商品名
	 */
	public boolean deleteGoodsById(int goodsId);
	
	/**
	 * 修改商品信息
	 */
	public boolean updateGoods( int goodsId, int typeId, String goodsName,
			double price, int minNumber, String remark);
	
	/**
	 * 通过商品名查询商品
	 * @param goodsName
	 * @return
	 */
	public Goods  getGoodsByName(String goodsName);
	
	
	/**
	 * 查询所有商品
	 * @return
	 */
	public List<Goods> getAllGoods();
	/**
	 * 通过商品类型查询商品列表
	 * @param typeId
	 * @return
	 */
	public List<Goods> getGoodsById(int typeId);
	
}
