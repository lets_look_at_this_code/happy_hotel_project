package cn.hotel.busi;

import java.util.Date;

import cn.hotel.entity.CheckOut;

/**
 * 退房结算业务层接口
 * @author 许立宏
 *
 */
public interface ICheckOutBusi {
	
	/**
	 * 向退房结算表中插入退房结算单据
	 * @param checkOut
	 * @return
	 */
	public boolean addCheckOut(CheckOut checkOut);
	
	/**
	 * 调换房间
	 * @param billNo
	 * @param roomNo
	 * @param price
	 * @return
	 */
	public boolean updateRoomNo(int billNo,int roomNo,double price);
	
	/**
	 * 顾客续住
	 * @param billNo
	 * @param days
	 * @param billAmount
	 * @return
	 */
	public boolean updateDays(int billNo,int days,double billAmount);
	
	/**
	 * 客户退房结算
	 * @param billNo
	 * @param outTime
	 * @param outDeposit
	 * @param billAmount
	 * @param state
	 * @return
	 */
	public boolean updateCheckOut(int billNo,Date outTime,int outDeposit,double billAmount,int state);
}
