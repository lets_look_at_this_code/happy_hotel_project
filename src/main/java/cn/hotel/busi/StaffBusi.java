package cn.hotel.busi;

import java.util.List;
import cn.hotel.entity.Staff;

public interface StaffBusi {
	/*
	 * 分页显示员工包括搜索功能
	 */
	List<Staff> getStaff(int pageIndex,int pageSize,String search);
	/*
	 * 获得员工总数
	 */
	int getStaffNum(String search);
	/*
	 * 修改员工
	 */
	boolean upDateStaff(int staffId,String staffName,int deptId,double salary);
	/*
	 * 删除员工
	 */
	boolean deleteStaff(int staffId);
	/*
	 * 添加员工
	 */
	boolean addStaff(Staff staff);
}
