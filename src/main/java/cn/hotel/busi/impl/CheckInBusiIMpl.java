package cn.hotel.busi.impl;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.ICheckInBusi;
import cn.hotel.dao.ICheckInDao;
import cn.hotel.entity.CheckIn;
import cn.hotel.util.BillNoUtil;
import cn.hotel.util.SqlSessionFactoryUtil;

/**
 * 入住登记业务层实现类
 * @author 许立宏
 *
 */
public class CheckInBusiIMpl implements ICheckInBusi {

	public CheckIn addCheckIn(CheckIn checkIn) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			ICheckInDao checkInDao = session.getMapper(ICheckInDao.class);
			int billNo = checkInDao.getMaxBillNo();
			String maxBillNo = Integer.toString(billNo);
			maxBillNo = BillNoUtil.getBillNo(maxBillNo);
			if(maxBillNo != null){
				billNo = Integer.parseInt(maxBillNo);
				checkIn.setBillNo(billNo);
			}
			int result = checkInDao.addCheckInBill(checkIn);
			if(result > 0){
				session.commit();
				return checkIn;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return null;
	}

	public boolean changeRoom(int billNo, int roomNo) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		boolean succ = false;
		try {
			ICheckInDao checkInDao = session.getMapper(ICheckInDao.class);
			CheckIn checkIn = new CheckIn();
			checkIn.setBillNo(billNo);
			checkIn.setRoomNo(roomNo);
			int result = checkInDao.updateCheckInBill(checkIn);
			if(result > 0){
				session.commit();
				succ = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return succ;
	}

	public int getMaxBillNo() {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		int maxBillNo = 0;
		try {
			ICheckInDao checkInDao = session.getMapper(ICheckInDao.class);
			maxBillNo = checkInDao.getMaxBillNo();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return maxBillNo;
	}

	public List<CheckIn> selectCheckInBill(Map<String, Object> map) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		List<CheckIn> list = null;
		try {
			ICheckInDao checkInDao = session.getMapper(ICheckInDao.class);
			list = checkInDao.selectCheckInBill(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return list;
	}

	public int selectBillCount(Map<String, Object> map) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		int count = 0;
		try {
			ICheckInDao checkInDao = session.getMapper(ICheckInDao.class);
			count = checkInDao.selectBillCount(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return count;
	}

}
