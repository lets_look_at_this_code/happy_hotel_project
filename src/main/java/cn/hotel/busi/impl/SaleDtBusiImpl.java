package cn.hotel.busi.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.ISaleDtBusi;
import cn.hotel.dao.IGoodsDao;
import cn.hotel.dao.ISaleDtDao;
import cn.hotel.entity.Goods;
import cn.hotel.entity.SaleDt;
import cn.hotel.entity.User;
import cn.hotel.util.SqlSessionFactoryUtil;

public class SaleDtBusiImpl implements ISaleDtBusi {

	public boolean addSaleGoods(int billNo, String goodsName, int num,
			int userId) {
		boolean flag = false;
		// 使用工具类获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		try {
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			Goods goods = goodsDao.getGoodsByName(goodsName);

			User user = new User(userId);
			SaleDt saleDt = new SaleDt(billNo, goods, num, goods.getPrice(),
					goods.getPrice() * num, user);

			ISaleDtDao saleDtDao = session.getMapper(ISaleDtDao.class);
			int result = saleDtDao.insertSaleGoods(saleDt);
			session.commit();
			if (result > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

	public List<SaleDt> getSaleDtByBillNo(int billNo) {
		List<SaleDt> list = null;
		// 使用工具类获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		try {
			ISaleDtDao saleDtDao = session.getMapper(ISaleDtDao.class);

			list = saleDtDao.getSaleDtByBillNo(billNo);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public boolean updateSaleDt(int billNo, String goodsName, int num,
			int userId) {
		boolean flag = false;
		// 使用工具类获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		try {
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			Goods goods = goodsDao.getGoodsByName(goodsName);

			User user = new User(userId);
			SaleDt saleDt = new SaleDt(billNo, goods, num, goods.getPrice(),
					goods.getPrice() * num, user);

			ISaleDtDao saleDtDao = session.getMapper(ISaleDtDao.class);

			int result = saleDtDao.updateSaleDt(saleDt);
			session.commit();

			if (result > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

	public SaleDt getOneSaleDt(int billNo, int goodsId) {
		SaleDt saleDt = null;
		// 使用工具类获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		try {
			ISaleDtDao saleDtDao = session.getMapper(ISaleDtDao.class);

			/*
			 * Map<String ,Integer> map = new HashMap<String, Integer>();
			 * map.put("billNo", billNo); map.put("goodsId", goodsId);
			 */

			saleDt = saleDtDao.getOneSaleDt(billNo, goodsId);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return saleDt;
	}

	public boolean deleteSaleDt(int billNo, int goodsId) {
		boolean flag = false;
		// 使用工具类获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		try {
			ISaleDtDao saleDtDao = session.getMapper(ISaleDtDao.class);

			int result = saleDtDao.deleteSaleDt(billNo, goodsId);
			session.commit();

			if (result > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}

}
