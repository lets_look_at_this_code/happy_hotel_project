package cn.hotel.busi.impl;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.IVipBusi;
import cn.hotel.dao.IVipDao;
import cn.hotel.entity.Vip;
import cn.hotel.util.SqlSessionFactoryUtil;

public class VipBusilmpl implements IVipBusi {

	public boolean addVip(String phone, String vipName, String sex,
			String identity, double countAmt, double balance, int gradeId) {
		boolean succ = false;
		SqlSession session = SqlSessionFactoryUtil.getSession();

		int result = 0;
		try {
			IVipDao vipDao = session.getMapper(IVipDao.class);
			Vip vip = new Vip(phone, vipName, sex, identity, countAmt, balance,
					gradeId);
			result = vipDao.addVip(vip);
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}	
		}
		if (result > 0) {
			succ = true;
		}
		return succ;
	}

	public boolean deleteVip(String phone) {
		boolean succ = false;
		SqlSession session = SqlSessionFactoryUtil.getSession();

		int result = 0;
		try {
			IVipDao vipDao = session.getMapper(IVipDao.class);
			
			result = vipDao.deleteVip(phone);
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}	
		}
		if (result > 0) {
			succ = true;
		}
		return succ;
	}

	public boolean updateVip(String phone, String vipName, String sex,
			String identity, double countAmt, double balance, int gradeId) {
		boolean succ = false;
		SqlSession session = SqlSessionFactoryUtil.getSession();

		int result = 0;
		try {
			IVipDao vipDao = session.getMapper(IVipDao.class);
			Vip vip = new Vip(phone, vipName, sex, identity, countAmt, balance,
					gradeId);
			result = vipDao.updateVip(vip);
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}	
		}
		if (result > 0) {
			succ = true;
		}
		return succ;
	}

	public Vip selectVip(String phone) {
		Vip vip = null;
		SqlSession session = SqlSessionFactoryUtil.getSession();

		
		try {
			IVipDao vipDao = session.getMapper(IVipDao.class);
			
			vip = vipDao.selectVip(phone);
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}	
		}
		return vip;
	}

}
