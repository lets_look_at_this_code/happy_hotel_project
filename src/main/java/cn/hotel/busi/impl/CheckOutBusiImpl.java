package cn.hotel.busi.impl;

import java.util.Date;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.ICheckOutBusi;
import cn.hotel.dao.ICheckOutDao;
import cn.hotel.entity.CheckOut;
import cn.hotel.util.SqlSessionFactoryUtil;

public class CheckOutBusiImpl implements ICheckOutBusi {

	public boolean addCheckOut(CheckOut checkOut) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		ICheckOutDao checkOutDao = session.getMapper(ICheckOutDao.class);
		int result = checkOutDao.addCheckOutBill(checkOut);
		boolean succ = false;
		if(result > 0){
			succ = true;
		}
		return succ;
	}

	public boolean updateRoomNo(int billNo, int roomNo, double price) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		ICheckOutDao checkOutDao = session.getMapper(ICheckOutDao.class);
		CheckOut checkOut = new CheckOut();
		checkOut.setBillNo(billNo);
		checkOut.setRoomNo(roomNo);
		checkOut.setPrice(price);
		int result = checkOutDao.updateRoomNo(checkOut);
		boolean succ = false;
		if(result > 0){
			succ = true;
		}
		return succ;
	}

	public boolean updateDays(int billNo, int days, double billAmount) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		ICheckOutDao checkOutDao = session.getMapper(ICheckOutDao.class);
		CheckOut checkOut = new CheckOut();
		checkOut.setBillNo(billNo);
		checkOut.setDays(days);
		checkOut.setBillAmount(billAmount);
		int result = checkOutDao.updateDays(checkOut);
		boolean succ = false;
		if(result > 0){
			succ = true;
		}
		return succ;
	}

	public boolean updateCheckOut(int billNo, Date outTime, int outDeposit,
			double billAmount, int state) {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		ICheckOutDao checkOutDao = session.getMapper(ICheckOutDao.class);
		CheckOut checkOut = new CheckOut();
		checkOut.setBillNo(billNo);
		checkOut.setOutTime(outTime);
		checkOut.setOutDeposit(outDeposit);
		checkOut.setBillAmount(billAmount);
		checkOut.setState(state);
		int result = checkOutDao.updateCheckOut(checkOut);
		boolean succ = false;
		if(result > 0){
			succ = true;
		}
		return succ;
	}
	

}
