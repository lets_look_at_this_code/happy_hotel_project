package cn.hotel.busi.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.StaffBusi;
import cn.hotel.dao.StaffDao;
import cn.hotel.entity.Department;
import cn.hotel.entity.Staff;

public class StaffBusiImpl implements StaffBusi {

	public List<Staff> getStaff(int pageIndex, int pageSize,String search) {
		//获得Session(工具类)
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		//创建Map
		Map map = new HashMap();
		//往Map存入参数
		map.put("pageIndex",pageIndex);
		map.put("pageSize",pageSize);
		map.put("search",search);
		StaffDao staff = session.getMapper(StaffDao.class);
		//修改数据库
		List<Staff> list = staff.getStaff(map);
		//返回集合
		return list;
	}
	public int getStaffNum(String search){
		//获得Session
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		StaffDao staff = session.getMapper(StaffDao.class);
		int count = staff.getNum(search);
		//返回参数
		return count;
	}
	public boolean upDateStaff(int staffId,String staffName,int deptId,double salary){
		//获得Session
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		//创建部门对象
		Department department = new Department(deptId);
		//创建员工对象
		Staff staff = new Staff(staffId,staffName,department,salary);
		StaffDao staffdao = session.getMapper(StaffDao.class);
		int count = staffdao.upDateStaff(staff);
		session.commit();
		if(count > 0){
			return true;
		}
		return false;
	}
	public boolean deleteStaff(int staffId){
		SqlSession session  = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		StaffDao staffdao = session.getMapper(StaffDao.class);
		int count = staffdao.deleteStaff(staffId);
		session.commit();
		if(count > 0)
			return true;
		return false;
	}
	public boolean addStaff(Staff staff){
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		StaffDao staffdao = session.getMapper(StaffDao.class);
		int count =  staffdao.addStaff(staff);
		session.commit();
		if(count > 0)
			return true;
		return false;
	}
}
