package cn.hotel.busi.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.IGoodsTypeBusi;
import cn.hotel.dao.IGoodsTypeDao;
import cn.hotel.entity.GoodsType;
import cn.hotel.util.SqlSessionFactoryUtil;

public class GoodsTypeBusiImpl implements IGoodsTypeBusi {

	public List<GoodsType> getGoodsType() {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		List<GoodsType> list = null;
		try {
			IGoodsTypeDao goodsTypeDao = session.getMapper(IGoodsTypeDao.class);
			list = goodsTypeDao.getGoodsType();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return list;
	}

}
