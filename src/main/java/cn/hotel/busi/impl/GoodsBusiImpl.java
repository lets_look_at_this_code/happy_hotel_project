package cn.hotel.busi.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.IGoodsBusi;
import cn.hotel.dao.IGoodsDao;
import cn.hotel.entity.Goods;
import cn.hotel.util.SqlSessionFactoryUtil;

/**
 * 
 * @author 王亚峰
 *
 */
public class GoodsBusiImpl implements IGoodsBusi {

	/*
	 * 添加商品信息(non-Javadoc)
	 * 
	 * @see cn.hotel.busi.IGoodsBusi#addGoods(int, java.lang.String, double,
	 * int, java.lang.String)
	 */
	public boolean addGoods(int typeId, String goodsName, double price,
			int minNumber, String remark) {
		// TODO Auto-generated method stub
		boolean flag = false;
		// 使用工具类获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			Goods goods = new Goods(0, typeId, goodsName, price, minNumber,
					remark);
			// 利用Session得到dao接口对象
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			// 调用dao接口的方法
			int result = goodsDao.addGoods(goods);
			// 对数据库的增删改操作,要进行提交
			session.commit();

			// 判断dao层返回值,给出busi层的返回值.
			if (result > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return flag;
	}

	/*
	 * 删除商品(non-Javadoc)
	 * 
	 * @see cn.hotel.busi.IGoodsBusi#deleteGoodsById(int)
	 */
	public boolean deleteGoodsById(int goodsId) {
		boolean flag = false;
		// 获取SqlSession
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			// 利用Session得到dao接口对象
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			// 调用dao接口的方法
			int result = goodsDao.deleteGoods(goodsId);
			// 对数据库的增删改操作,要进行提交
			session.commit();

			if (result > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return flag;
	}

	/*
	 * 修改商品信息(non-Javadoc)
	 * 
	 * @see cn.hotel.busi.IGoodsBusi#updateGoods(int, int, java.lang.String,
	 * double, int, java.lang.String)
	 */
	public boolean updateGoods(int goodsId, int typeId, String goodsName,
			double price, int minNumber, String remark) {
		boolean flag = false;
		// 获取Sqlsession
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			Goods goods = new Goods(goodsId, typeId, goodsName, price,
					minNumber, remark);
			// 利用session得到到接口对象
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			// 调用dao接口的方法
			int result = goodsDao.updateGoods(goods);
			// 对数据库的增删改操作,要进行提交
			session.commit();

			if (result > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return flag;
	}

	/*
	 * 查询部分商品信息(non-Javadoc)
	 * 
	 * @see cn.hotel.busi.IGoodsBusi#getGoodsByName(java.lang.String)
	 */
	public Goods getGoodsByName(String goodsName) {
		// 获取Sqlsession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		Goods goods = null;
		try {
			// 利用session得到接口对象
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			// 调用dao接口的方法,获取goods对象
			goods = goodsDao.getGoodsByName(goodsName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return goods;
	}

	/*
	 * 查询所有商品信息(non-Javadoc)
	 * 
	 * @see cn.hotel.busi.IGoodsBusi#getAllGoods()
	 */
	public List<Goods> getAllGoods() {
		// 获取Sqlsession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		List<Goods> list = null;
		try {
			// 利用session得到接口对象
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			// 调用dao接口的方法,获取goods对象
			list = goodsDao.getAllGoods();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return list;
	}

	public List<Goods> getGoodsById(int typeId) {
		// 获取Sqlsession
		SqlSession session = SqlSessionFactoryUtil.getSession();

		List<Goods> list = null;
		try {
			// 利用session得到接口对象
			IGoodsDao goodsDao = session.getMapper(IGoodsDao.class);
			// 调用dao接口的方法,获取goods对象
			list = goodsDao.getGoodsById(typeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return list;
	}

}
