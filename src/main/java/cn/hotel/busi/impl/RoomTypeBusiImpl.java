package cn.hotel.busi.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.IRoomTypeBusi;
import cn.hotel.dao.IRoomDao;
import cn.hotel.dao.IRoomTypeDao;
import cn.hotel.entity.Room;
import cn.hotel.entity.RoomType;
import cn.hotel.util.SqlSessionFactoryUtil;

public class RoomTypeBusiImpl implements IRoomTypeBusi {
	public boolean addRoomtype(int typeId,String typeName,double price,String description){
		boolean flag = false;
		SqlSession session = SqlSessionFactoryUtil.getSession();
		//利用getMapper(接口类的.class文件),获得接口对象
		try {
			IRoomTypeDao roomtypeDao = session.getMapper(IRoomTypeDao.class);
			//调用接口对象具体方法操作数据库
			RoomType roomtype = new RoomType(typeId,typeName,price, description);
			int result = roomtypeDao.addRoomtype(roomtype);
			//对数据库的增删改操作 要加入session.commit才能成功
			session.commit();
			if(result>0){
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			if(session != null){
				session.close();
			}
		}
		return flag;
	}

	public boolean deleteRoomType(int typeId) {
		// TODO Auto-generated method stub
		boolean flag = false;
		//得到session
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			//利用getMapper(的.class文件)获得接口方法
			IRoomTypeDao roomtypeDao = session.getMapper(IRoomTypeDao.class);
			//调用接口对象的具体方法操作数据库
			int result = roomtypeDao.deleteRoomtype(typeId);
			//调用commit成功操作数据库
			session.commit();
			if(result>0){
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			if(session != null){
				session.close();
			}
		}
		return flag;
	}

	public RoomType queryRoomType(int typeId) {
		// TODO Auto-generated method stub
		SqlSession session = SqlSessionFactoryUtil.getSession();
		RoomType roomtype = null;
		try {
			//利用getMapper(的.class文件)获得接口方法
			IRoomTypeDao roomtypeDao = session.getMapper(IRoomTypeDao.class);
			//调用接口对象的具体方法操作数据库
			roomtype = roomtypeDao.queryRoomtype(typeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return roomtype;
	}
	public List<RoomType> queryRoomtype() {
		SqlSession session = SqlSessionFactoryUtil.getSession();
		List<RoomType> list = null;
		try {
			//利用getMapper(的.class文件)获得接口方法
			IRoomTypeDao roomtypeDao = session.getMapper(IRoomTypeDao.class);
			//调用接口对象的具体方法操作数据库
			list = roomtypeDao.queryRoomtype();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return list;
	}
	
	public boolean alterRoomtype(int typeId,String typeName,double price,String description){
		boolean flag = false;
		SqlSession session = SqlSessionFactoryUtil.getSession();
		//利用getMapper(接口类的.class文件),获得接口对象
		try {
			IRoomTypeDao roomtypeDao = session.getMapper(IRoomTypeDao.class);
			//调用接口对象具体方法操作数据库
			RoomType roomtype = new RoomType(typeId,typeName,price, description);
			int result = roomtypeDao.alterRoomtype(roomtype);
			//对数据库的增删改操作 要加入session.commit才能成功
			session.commit();
			if(result>0){
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			if(session != null){
				session.close();
			}
		}
		return flag;
	}
}
