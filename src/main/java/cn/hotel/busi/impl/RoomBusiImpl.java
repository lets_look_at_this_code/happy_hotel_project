package cn.hotel.busi.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.IRoomBusi;
import cn.hotel.dao.IRoomDao;
import cn.hotel.entity.Room;
import cn.hotel.util.SqlSessionFactoryUtil;

public class RoomBusiImpl implements IRoomBusi {

	public boolean addRoom(int roomNo, int typeId, int state, String remark) {
		boolean flag = false;
		//得到session
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			//利用getMapper(接口类的.class文件),获得接口对象
			IRoomDao roomDao = session.getMapper(IRoomDao.class);
			//调用接口对象具体方法操作数据库
			Room room  = new Room(roomNo, typeId, state, remark);
			int result = roomDao.addRoom(room);
			//对数据库的增删改操作 要加入session.commit才能成功
			session.commit();
			if(result>0){
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session!=null){
				session.close();
			}
		}
		return flag;
	}
	
	public boolean deleteRoom(int roomNo){
		boolean flag = false;
		//得到session
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			//利用getMapper(的.class文件)获得接口方法
			IRoomDao roomDao = session.getMapper(IRoomDao.class);
			//调用接口对象的具体方法操作数据库
			int result = roomDao.deleteRoom(roomNo);
			//调用commit成功操作数据库
			session.commit();
			if(result>0){
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			if(session != null){
				session.close();
			}
		}
		return flag;
	}
	
	public Room queryRoom(int roomNo){
		SqlSession session = SqlSessionFactoryUtil.getSession();
		Room room = null;
		try {
			//利用getMapper(的.class文件)获得接口方法
			IRoomDao roomDao = session.getMapper(IRoomDao.class);
			//调用接口对象的具体方法操作数据库
			room = roomDao.queryRoom(roomNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return room;
	}
	
	public List<Room> getRoomByTypeId(int typeId){
		List<Room> list = null;
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			//利用getMapper(的.class文件)获得接口方法
			IRoomDao roomDao = session.getMapper(IRoomDao.class);
			//调用接口对象的具体方法操作数据库
			
			
			list = roomDao.getRoomByTypeId(typeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session != null){
				session.close();
			}
		}
		return list;
	}
	
	public boolean alterRoom(int roomNo, int typeId, int state, String remark){
		boolean flag = false;
		//得到session
		SqlSession session = SqlSessionFactoryUtil.getSession();
		try {
			//利用getMapper(接口类的.class文件),获得接口对象
			IRoomDao roomDao = session.getMapper(IRoomDao.class);
			//调用接口对象具体方法操作数据库
			Room room = new Room(roomNo, typeId, state, remark);
			int result = roomDao.alterRoom(room);
			//对数据库的增删改操作 要加入session.commit才能成功
			session.commit();
			if(result>0){
				flag = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(session!=null){
				session.close();
			}
		}
		return flag;
	}
	
	
}
