package cn.hotel.busi.impl;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.InventoryBusi;
import cn.hotel.dao.InventoryDao;
import cn.hotel.entity.Inventory;
import cn.hotel.util.SqlSessionFactoryUtil;

public class InventoryBusiImpl implements InventoryBusi {

	public boolean addInventory(Inventory inven) {
		boolean success = false;
		SqlSession se = null;
		try {

			se = SqlSessionFactoryUtil.getSession();
			InventoryDao dao = se.getMapper(InventoryDao.class);
			int it = dao.addInventory(inven);
			se.commit();
			if (it > 0) {
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (se != null)
				se.close();
		}

		return success;
	}

	public boolean deletInventory(Inventory inven) {
		boolean success = false;
		SqlSession se = null;
		try {

			se = SqlSessionFactoryUtil.getSession();
			InventoryDao dao = se.getMapper(InventoryDao.class);
			int it = dao.deleteInventory(inven);
			if (it > 0) {
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (se != null)
				se.close();
		}

		return success;
	}

	public Inventory searchInventory(int goodsId) {
		Inventory inven = null;
		SqlSession se = null;
		try {

			se = SqlSessionFactoryUtil.getSession();
			InventoryDao dao = se.getMapper(InventoryDao.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (se != null)
				se.close();
		}
			
		return inven;
	}

	public boolean updateInventoryNum(int num) {
		boolean success = false;
		SqlSession se = null;
		try {

			se = SqlSessionFactoryUtil.getSession();
			InventoryDao dao = se.getMapper(InventoryDao.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (se != null)
				se.close();
		}
		return false;
	}

}
