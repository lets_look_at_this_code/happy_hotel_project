package cn.hotel.busi.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import cn.hotel.busi.IDeptBusi;
import cn.hotel.dao.IDeptDao;
import cn.hotel.entity.Department;

public class IDeptBusiImpl implements IDeptBusi {

	public List<Department> getDept() {
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		IDeptDao dept = session.getMapper(IDeptDao.class);
		List<Department> list = dept.getDept();
		return list;
	}
	public boolean deleteDepartment(int deptId){
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		IDeptDao dept = session.getMapper(IDeptDao.class);
		int count = dept.deleteDepartment(deptId);
		session.commit();
		if(count > 0)
			return true;
		return false;
	}
	public boolean updateDepartment(int deptId,String deptName) {
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		IDeptDao dept = session.getMapper(IDeptDao.class);
		Department department = new Department(deptId,deptName);
		int count = dept.updateDepartment(department);
		session.commit();
		if(count > 0)
			return true;
		return false;
	}
	public boolean addDepartment(Department department){
		SqlSession session = cn.hotel.util.SqlSessionFactoryUtil.getSession();
		IDeptDao dept = session.getMapper(IDeptDao.class);
		int count = dept.addDepartment(department);
		session.commit();
		if(count > 0)
			return true;
		return false;
	}
}
