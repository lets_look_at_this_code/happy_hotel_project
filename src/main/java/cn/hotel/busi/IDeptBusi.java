package cn.hotel.busi;

import java.util.List;
import cn.hotel.entity.Department;

public interface IDeptBusi {
	/*
	 * 获得部门集合
	 */
	List<Department> getDept();
	/*
	 * 删除部门
	 */
	boolean deleteDepartment(int deptId);
	/*
	 * 修改部门
	 */
	boolean updateDepartment(int deptId,String deptName);
	/*
	 * 添加部门
	 */
	boolean addDepartment(Department department);
}
