package cn.hotel.busi;

import java.util.List;

import cn.hotel.entity.SaleDt;

/**
 * @version 1.0
 * @author 项目一组--王立申
 *
 */
public interface ISaleDtBusi {
	/**
	 * @param saleDt 一条销售商品信息
	 * @return 是否插入成功 0代表失败，1代表成功
	 */
	public boolean addSaleGoods(int billNo,String goodsName,int num,int userId);
	
	/**
	 * 通过住宿编号查询同一个订单下的所有商品列表
	 * @param billNo 住宿编号
	 * @return  装有多条销售商品信息的list集合
	 */
	List<SaleDt> getSaleDtByBillNo(int billNo);
	
	/**
	 * 对一条销售商品信息进行更改，主要变更同一商品的销售数量和总价
	 * @param   
	 * @return
	 */
	boolean updateSaleDt(int billNo,String goodsName,int num,int userId);
	/**
	 * 通过 订单号和商品id 获取单条销售商品信息，
	 * @param billNo
	 * @param goodsId
	 * @return
	 */
	SaleDt getOneSaleDt(int billNo,int goodsId);
	/**
	 * 根据订单编号和商品Id删除一条商品信息
	 * @param billNo 
	 * @param goodsId
	 * @return
	 */
	boolean deleteSaleDt(int billNo,int goodsId);
}
