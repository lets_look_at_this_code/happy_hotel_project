package cn.hotel.busi;

import cn.hotel.entity.Vip;

/**
 * 会员信息接口
 * @author 张瑞峰
 *
 */

public interface IVipBusi {
	
	/**
	 * 添加会员信息
	 * @param vip 对象
	 * @return 返回插入的结果
	 */
	public boolean addVip(String phone, String vipName, String sex, String identity,
			double countAmt, double balance, int gradeId);
	
	/**
	 * 根据手机号删除会员信息
	 * @param 手机号
	 * @return 返回删除结果
	 */
	public boolean deleteVip(String phone);
	
	/**
	 * 修改会员信息
	 * @param vip 对象
	 * @return 返回修改结果
	 */
	public boolean updateVip(String phone, String vipName, String sex, String identity,
			double countAmt, double balance, int gradeId);
	
	/**
	 * 查看会员信息
	 * @param phone 手机号
	 * @return 返回查询结果
	 */
	public Vip selectVip(String phone);
}
