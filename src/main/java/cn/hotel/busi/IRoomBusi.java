package cn.hotel.busi;

import java.util.List;

import cn.hotel.entity.Room;

/**
 * 操控房间信息表dao层接口对数据库更改
 * @author 朱康康
 *
 */
public interface IRoomBusi {
	/**
	 * 增加房间
	 * @param roomNo
	 * @param typeId
	 * @param state
	 * @param ramark
	 * @return
	 */
	boolean addRoom(int roomNo,int typeId,int state,String ramark);
	/**
	 * 删除房间
	 * @param roomNo
	 * @return
	 */
	boolean deleteRoom(int roomNo);
	/**
	 * 通过房间号查询房间
	 * @param roomNo
	 * @return
	 */
	Room queryRoom(int roomNo);
	/**
	 * 通过房间类型查询房间
	 * @param typeId
	 * @return
	 */
	List<Room> getRoomByTypeId(int typeId);
	/**
	 * 修改房间信息
	 * @param roomNo
	 * @param typeId
	 * @param state
	 * @param ramark
	 * @return
	 */
	boolean alterRoom(int roomNo, int typeId, int state, String ramark);
	
}
