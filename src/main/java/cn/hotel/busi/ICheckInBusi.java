package cn.hotel.busi;

import java.util.List;
import java.util.Map;

import cn.hotel.entity.CheckIn;


/**
 * 入住登记业务层接口
 * @author 许立宏
 *
 */
public interface ICheckInBusi {

	/**
	 * 添加入住登记
	 * @param checkIn
	 * @return 返回插入结果checkIn
	 */
	public CheckIn addCheckIn(CheckIn checkIn);
	
	/**
	 * 换房
	 * @param billNo 住宿登记编号
	 * @param roomNo 原房间号
	 * @return 返回数据插入结果
	 */
	public boolean changeRoom(int billNo, int roomNo);
	
	/**
	 * 获取入住登记表的最大billno
	 * @return
	 */
	public int getMaxBillNo();
	
	/**
	 * 根据条件查询入住登记单据
	 * @param map
	 * @return
	 */
	public List<CheckIn> selectCheckInBill(Map<String, Object> map);
	
	/**
	 * 根据条件查询总单据数
	 * @param map
	 * @return
	 */
	public int selectBillCount(Map<String, Object> map);
}
