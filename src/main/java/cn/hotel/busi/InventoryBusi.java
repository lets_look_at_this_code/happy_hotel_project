package cn.hotel.busi;

import cn.hotel.entity.Inventory;

public interface InventoryBusi {
	/**
	 * 向库存表中添加商品
	 * @param inven
	 * @return boolean
	 */
	public boolean addInventory(Inventory inven);

	/**
	 * 删除库存表中的商品
	 * @param inven
	 * @return boolean
	 */
	public boolean deletInventory(Inventory inven);
	
	/**
	 * 根据商品Id查找库存表中是否已存在该商品
	 * @param inven
	 * @return
	 */
	public Inventory searchInventory(int goodsId);
	
	/**
	 * 修改指定商品的数量
	 * @param num
	 * @return
	 */
	public boolean updateInventoryNum(int num);
}
